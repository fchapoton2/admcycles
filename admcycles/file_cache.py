# -*- coding: utf-8 -*-
import os
import pickle
import bz2
import warnings
import urllib.parse
from urllib.request import urlretrieve, urlopen
from urllib.error import HTTPError
import configparser
import string

from sage.rings.rational_field import QQ
from sage.modules.free_module_element import vector
from sage.misc.cachefunc import CachedFunction, dict_key
from sage.misc.decorators import decorator_keywords
try:
    # NOTE: moved in sage 9.7
    from sage.misc.instancedoc import instancedoc
except ImportError:
    from sage.docs.instancedoc import instancedoc


@instancedoc
class FileCachedFunction(CachedFunction):
    r"""
    Function wrapper that implements a cache extending SageMath's CachedFunction.

    Preface a function definition with @file_cached_function to wrap it.  When
    the wrapped function is called the following locations are visited to try
    and obtain the output data:

    - first the cache in working memory
    - then the local filesystem
    - then the internet If the data is not stored in any of these locations
      then the wrapped function is executed.  The output of the function is
      saved in the working memory cache and as a file in the filesystem.

    By default, the file is saved in the directory given by the directory
    argument in the current working directory.  If a name for a environment
    variable is supplied via the env_var argument and this environment variable
    is set to a valid path, then this director is used instead of the current
    working directory.

    A filename is generated from the function arguments.  The default
    implementation requires that:

    - the arguments are hashable and convertible to strings via str(),
    - the resulting strings do not contain any characters not allowed in file
      names.

    The remote_database_list accepts the url of a file that should contain a
    list of all files in the remote cache, one file per line.
    This allows bulk downloading the files with the download_all() method.

    The key argument accepts a callable to generate the cache key from the
    function arguments. For details see the documentation of CachedFunction.

    The filename argument accepts a callable that generates the file name from
    the function name and the key.  Whenever key is provided, filename must be
    provided, too.

    The behavior of ``FileCacheFunction`` can be modified via boolean
    configuration variables :meth:`set_config` and :meth:`get_config`. The
    available configuration variables are

    - ``'read_cache'``: whether to look in the cache dictionary for values

    - ``'write_cache'``: whether to add newly computed values in the cache dictionary

    - ``'read_file'``: whether to read values from cached files (stored from previous
      sessions or downloaded from internet)

    - ``'write_file'``: whether to write files for newly computed values (and made
      computations available in any future sessions)

    - ``'online_lookup'``: whether to look online for values

    For example, if all of ``'read_file'``, ``'write_file'`` and
    ``'online_lookup'`` are set to ``False`` then the behaviour of
    ``FileCachedFunction`` is similar to the SageMath ``CachedFunction``.

    EXAMPLES::

        sage: from admcycles.file_cache import file_cached_function, ignore_args_key, ignore_args_filename
        sage: from tempfile import mkdtemp
        sage: from shutil import rmtree
        sage: import os

        sage: tmpdir = mkdtemp()
        sage: # We ignore the second argument for caching
        sage: @file_cached_function(directory=tmpdir, key=ignore_args_key([1]), filename=ignore_args_filename())
        ....: def f(a, b=True):
        ....:     pass
        sage: f(1)
        sage: assert os.path.exists(os.path.join(tmpdir, "f_1.pkl.bz2"))
        ....:
        sage: os.environ["TEST_CACHE_DIR"] = tmpdir
        sage: @file_cached_function(directory="", env_var="TEST_CACHE_DIR")
        ....: def f(a, b=True):
        ....:     pass
        sage: f(1)
        sage: assert os.path.exists(os.path.join(tmpdir, "f_1_True.pkl.bz2"))
        sage: rmtree(tmpdir)
    """
    config_options = ('cache_read', 'cache_write', 'file_read', 'file_write', 'online_lookup')

    def __init__(self, f, directory, url=None, remote_database_list=None, env_var=None, key=None, filename=None, pickle_wrappers=(None, None)):
        self.env_var = env_var
        if env_var is not None:
            try:
                env_dir = os.environ[env_var]
                if not os.path.isdir(env_dir):
                    warnings.warn("%s=%s is not a directory. Ignoring it." % env_var, env_dir)
                else:
                    directory = os.path.join(env_dir, directory)
            except KeyError:
                pass

        if key is not None and filename is None:
            raise ValueError("If key is provided, filename must also be provided")

        super(FileCachedFunction, self).__init__(f, key=key)
        if directory is not None:
            os.makedirs(directory, exist_ok=True)

        self.directory = directory
        self.url = url
        self.remote_database_list = remote_database_list
        self.filename = filename
        self.pickle_wrapper, self.unpickle_wrapper = pickle_wrappers

        self.cache_read = True
        self.cache_write = True
        self.file_read = True
        self.file_write = True
        self.online_lookup = True
        self.read_config()

        if self.directory is None:
            self.file_read = False
            self.file_write = False

        if self.url is None:
            self.online_lookup = False

    def __call__(self, *args, **kwds):
        k = self.get_key(*args, **kwds)

        # First try to return the value from the cache.
        if self.cache_read:
            try:
                return self.cache[k]
            except TypeError:  # k is not hashable
                k = dict_key(k)
                try:
                    return self.cache[k]
                except KeyError:
                    pass
            except KeyError:
                pass

        # The value is not in the cache, check whether the cache file exists.
        # If it does not, attempt downloading it.
        if self.file_read:
            filename, filename_with_path = self.filename_from_args(k)
            if not os.path.exists(filename_with_path) and self.online_lookup:
                try:
                    self.__download(filename, filename_with_path)
                except IOError:
                    pass

            # If the cache file exists now, load it.
            if os.path.exists(filename_with_path):
                try:
                    dat = self.__load_from_file(filename_with_path)
                    if self.cache_write:
                        self.cache[k] = dat
                    return dat
                except IOError:
                    pass
                except TypeError:
                    warnings.warn("can not unpickle file %s, it was probably created with a newer version of SageMath.")

        # All methods to retrive the data from the cache have failed.
        dat = self.f(*args, **kwds)
        self.__save(k, dat)
        return dat

    def set_config(self, **kwds):
        r"""
        Set the boolean value of one or several configuration variable(s).

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: import os

            sage: tmpdir = mkdtemp()

            sage: @file_cached_function(directory=tmpdir)
            ....: def my_function(a, b):
            ....:     return a * b
            sage: my_function(2, 3)
            6
            sage: my_function.cached_filenames()
            ['my_function_2_3.pkl.bz2']

        Now we deactivate writing files on disk and call a new value::

            sage: my_function.set_config(file_write=False)
            sage: my_function(3, 3)
            9
            sage: my_function.cached_filenames()
            ['my_function_2_3.pkl.bz2']

        Activating again::

            sage: my_function.set_config(file_write=True)
            sage: my_function(3, 5)
            15
            sage: my_function.cached_filenames()
            ['my_function_2_3.pkl.bz2', 'my_function_3_5.pkl.bz2']

        Note that the value for arguments ``(3, 3)`` is already in the cache
        and won't be saved in a file on subsequent calls. You need to
        explicitely clear the cache to make that happen::

            sage: my_function(3, 3)
            9
            sage: my_function.cached_filenames()
            ['my_function_2_3.pkl.bz2', 'my_function_3_5.pkl.bz2']
            sage: my_function.clear_cache()
            sage: my_function(3, 3)
            9
            sage: my_function.cached_filenames()
            ['my_function_2_3.pkl.bz2', 'my_function_3_3.pkl.bz2', 'my_function_3_5.pkl.bz2']
        """
        for key, value in kwds.items():
            if key not in self.config_options:
                raise KeyError('invalid configuration keyword {!r}'.format(key))
            if not isinstance(value, bool):
                raise ValueError('invalid configuration value={!r}; must be a boolean'.format(value))
            if (key == 'file_read' or key == 'file_write') and value and self.directory is None:
                raise ValueError('no directory set; can not turn file_read or file_write to True')
            if key == 'online_lookup' and value and self.url is None:
                raise ValueError('no url set; can not turn online_lookup to True')
            setattr(self, key, value)

    def get_config(self, key):
        r"""
        Get the boolean value of the configuration variable ``key``.

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: import os

            sage: tmpdir = mkdtemp()

            sage: @file_cached_function(directory=tmpdir)
            ....: def my_function1(a, b):
            ....:     return a * b
            sage: my_function1.get_config('cache_read')
            True
            sage: my_function1.set_config(cache_read=False)
            sage: my_function1.get_config('cache_read')
            False
        """
        if key not in self.config_options:
            raise KeyError('invalid configuration keyword {!r}'.format(key))
        return getattr(self, key)

    def pprint_config(self, **kwds):
        r"""
        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: import os

            sage: tmpdir = mkdtemp()

            sage: @file_cached_function(directory=tmpdir)
            ....: def my_function1(a, b):
            ....:     return a * b
            sage: my_function1.pprint_config()
            {
             cache_read: True,
             cache_write: True,
             file_read: True,
             file_write: True,
             online_lookup: False
            }
            sage: my_function1.set_config(file_write=False)
            sage: my_function1.pprint_config()
            {
             cache_read: True,
             cache_write: True,
             file_read: True,
             file_write: False,
             online_lookup: False
            }
        """
        print('{')
        print('  cache_read: %s,' % self.cache_read)
        print('  cache_write: %s,' % self.cache_write)
        print('  file_read: %s,' % self.file_read)
        print('  file_write: %s,' % self.file_write)
        print('  online_lookup: %s' % self.online_lookup)
        print('}')

    def read_config(self, filename=None):
        r"""
        Load the configuration from a file.

        By default the configuration file is looked for in the directory
        where the data is saved. It is also possible to provide a custom
        ``filename`` argument.

        See also :meth:`read_config`.
        """
        config = configparser.ConfigParser()
        if filename is None:
            filename = os.path.join(self.directory, self.f.__name__ + ".conf")
            if not os.path.isfile(filename):
                return
        elif not os.path.isfile(filename):
            raise ValueError('invalid filename {!r}'.format(filename))

        config.read(filename)
        for key in self.config_options:
            value = config.get('general', key, fallback=None)
            if value is not None:
                self.__dict__[key] = value == 'yes'

    def save_config(self):
        r"""
        Save the current configuration in the configuration file.

        See also :meth:`read_config`.

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: import os

            sage: tmpdir = mkdtemp()

            sage: @file_cached_function(directory=tmpdir)
            ....: def my_function(a, b):
            ....:     return a * b
            sage: my_function.set_config(cache_read=False)
            sage: my_function.set_config(cache_write=False)
            sage: my_function.save_config()
            sage: del my_function
            sage: @file_cached_function(directory=tmpdir)
            ....: def my_function(a, b):
            ....:     return a * b
            sage: my_function.pprint_config()
            {
             cache_read: False,
             cache_write: False,
             file_read: True,
             file_write: True,
             online_lookup: False
            }
        """
        if self.directory is None:
            raise ValueError('no directory set')
        config = configparser.ConfigParser()
        config["general"] = {'cache_read': 'yes' if self.cache_read else 'no',
                             'cache_write': 'yes' if self.cache_write else 'no',
                             'file_read': 'yes' if self.file_read else 'no',
                             'file_write': 'yes' if self.file_write else 'no',
                             'online_lookup': 'yes' if self.online_lookup else 'no'}
        filename = os.path.join(self.directory, self.f.__name__ + ".conf")
        with open(filename, "w") as f:
            config.write(f)

    def __get_online_lookup_default(self):
        r"""
        Tries to obtain a user specified value from txe config file.
        Returns True if this is not possible.
        """
        from .superseded import deprecation
        deprecation(225, '__get_online_lookup_default is deprecated; use get_config instead')
        return self.get_config('online_lookup')

    def set_online_lookup_default(self, b):
        r"""
        Deprecated.

        Superseded by :meth:`set_config` and :meth:`save_config`.
        """
        from .superseded import deprecation
        deprecation(225, 'set_online_lookup_default is deprecated; use set_config and save_config instead')
        # TODO: deprecation
        if b and self.url is None:
            raise ValueError("no online database available for this function")
        self.set_config(online_lookup=b)
        self.save_config()

    def set_online_lookup(self, b):
        r"""
        Deprecated.

        Superseded by :meth:`set_config`.
        """
        if b and self.url is None:
            raise ValueError("no online database available for this function")
        from .superseded import deprecation
        deprecation(225, 'set_online_lookup is deprecated; use set_config instead')
        self.set_config(online_lookup=b)

    def set_cache(self, dat, *args, **kwds):
        r"""
        Manually add a value to the cache.

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: from shutil import rmtree
            sage: tmpdir = mkdtemp()
            sage: @file_cached_function(directory=tmpdir)
            ....: def f(a, b=True):
            ....:     pass
            sage: f.set_cache("test", 1, b=False)
            sage: assert f(1, False) == "test"  # This is the cached value
            sage: f.clear_cache()
            sage: f(1, False)
            'test'
            sage: rmtree(tmpdir)

            The above output "test" is the file cached value, as f returns None.
        """
        k = self.get_key(*args, **kwds)
        self.__save(k, dat)

    def __create_directory(self):
        r"""
        Deprecated.
        """
        from .superseded import deprecation
        deprecation(225, '__create_directory is deprecated and does nothing')

    def __save(self, k, dat):
        r"""
        Saves the data in the cache file and the in-memory cache.

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: from shutil import rmtree
            sage: tmpdir = mkdtemp()
            sage: @file_cached_function(directory=tmpdir)
            ....: def f(a, b=True):
            ....:     pass
            sage: k = f.get_key(1)
            sage: f._FileCachedFunction__save(k, "test")
            sage: assert f.cache[k] == "test"
            sage: f.clear_cache()
            sage: f(1)
            'test'
            sage: rmtree(tmpdir)

            The above "test" is the file cached value, as f returns None.
        """
        if self.cache_write:
            self.cache[k] = dat

        if self.file_write:
            filename, filename_with_path = self.filename_from_args(k)
            with bz2.open(filename_with_path, 'wb') as f:
                # We force pickle to use protocol version 3 to make
                # sure that it works for all Python 3 version
                # See
                # https://docs.python.org/3/library/pickle.html
                if self.pickle_wrapper is not None:
                    dat = self.pickle_wrapper(dat)
                pickle.dump(dat, f, protocol=3)

    def cached_filenames(self, relative=True):
        r"""
        Iterate through the names of files that were cached.

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: import os

            sage: tmpdir = mkdtemp()

            sage: @file_cached_function(directory=tmpdir)
            ....: def my_function(a, b):
            ....:     return a * b
            sage: my_function(2, 3)
            6
            sage: my_function.cached_filenames()
            ['my_function_2_3.pkl.bz2']
        """
        output = []
        for filename in os.listdir(self.directory):
            full_filename = os.path.join(self.directory, filename)
            if (os.path.isfile(full_filename) and
               filename.endswith('.pkl.bz2') and
               filename.startswith(self.f.__name__)):
                output.append(filename if relative else full_filename)
        output.sort()
        return output

    def clear_cached_files(self):
        r"""
        Remove cached data from the disk.

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: from tempfile import mkdtemp
            sage: import os

            sage: tmpdir = mkdtemp()

            sage: @file_cached_function(directory=tmpdir)
            ....: def my_function(a, b):
            ....:     return a * b
            sage: my_function(2, 3)
            6
            sage: my_function.cached_filenames()
            ['my_function_2_3.pkl.bz2']
            sage: my_function.clear_cached_files()
            sage: my_function.cached_filenames()
            []
        """
        for filename in self.cached_filenames(relative=False):
            os.remove(filename)

    def filename_from_args(self, k):
        r"""
        Construct a file name of the form func_name_arg1_arg2_arg3.pkl.bz2

        EXAMPLES::

            sage: from admcycles.file_cache import file_cached_function
            sage: @file_cached_function(directory="dir")
            ....: def f(a, b=True):
            ....:     pass
            sage: k = f.get_key(1)
            sage: f.filename_from_args(k)
            ('f_1_True.pkl.bz2', 'dir/f_1_True.pkl.bz2')
        """
        if self.filename is None:
            filename = self.f.__name__
            for a in k[0]:
                filename += '_' + str(a)
            filename += '.pkl.bz2'
        else:
            filename = self.filename(self.f, k)
        filename_with_path = os.path.join(self.directory, filename)
        return (filename, filename_with_path)

    def __load_from_file(self, filename_with_path):
        r"""
        Unplickles the given file and returns the data.
        """
        with bz2.open(filename_with_path, 'rb') as file:
            if self.unpickle_wrapper is None:
                return pickle.load(file)
            else:
                return self.unpickle_wrapper(pickle.load(file))

    def __download(self, filename, filename_with_path):
        r"""
        Download the given file from the remote database and stores it
        on the file system.
        """
        if self.url is None:
            raise ValueError('no url provided')
        complete_url = urllib.parse.urljoin(self.url, filename)
        try:
            urlretrieve(complete_url, filename_with_path)
        except HTTPError:
            pass

    def download_all(self):
        r"""
        Download all files from the remote database.
        """
        if self.url is None:
            raise ValueError('no url provided')
        if self.remote_database_list is None:
            raise ValueError('no remote database list provided')
        try:
            for filename in urlopen(self.remote_database_list):
                filename = filename.decode('utf-8').strip()
                # Check that the filename does not contain any characters that
                # may result in downloading from or saving to an unwanted location.
                allowed = set(string.ascii_letters + string.digits + '.' + '_')
                if not set(filename) <= allowed:
                    print("Received an invalid filename, aborting.")
                    return
                filename_with_path = os.path.join(self.directory, filename)
                if not os.path.exists(filename_with_path):
                    print("Downloading", filename)
                    self.__download(filename, filename_with_path)
        except HTTPError as e:
            print("Can not open", self.remote_database_list, e)


file_cached_function = decorator_keywords(FileCachedFunction)


def ignore_args_key(ignore_args):
    r"""
    Returns a callable that builds a key from a list of arguments,
    but ignores the arguments with the indices supplied by ignore_arguments.

    EXAMPLES::

        sage: from admcycles.file_cache import ignore_args_key
        sage: key = ignore_args_key([0, 1])
        sage: key("first arg", "second arg", "third arg")
        ('third arg',)
    """
    def key(*args, **invalid_args):
        return tuple(arg for i, arg in enumerate(args) if i not in ignore_args)

    return key


def ignore_args_filename():
    r"""
    Returns a callable that builds a file name from the key returned by
    ignore_args_key.

    EXAMPLES::

        sage: from admcycles.file_cache import ignore_args_key, ignore_args_filename
        sage: key = ignore_args_key([0, 1])
        sage: filename = ignore_args_filename()
        sage: def test():
        ....:     pass
        sage: filename(test, key("first arg", "second arg", "third arg"))
        'test_third arg.pkl.bz2'
    """
    def filename(f, key):
        filename = f.__name__
        for a in key:
            filename += '_' + str(a)
        filename += '.pkl.bz2'
        return filename

    return filename


def rational_to_py(q):
    r"""
    Converts a rational number to a pair of python integers.

    EXAMPLES::

        sage: from admcycles.file_cache import rational_to_py
        sage: a, b = rational_to_py(QQ(1/2))
        sage: a
        1
        sage: type(a)
        <class 'int'>
        sage: b
        2
        sage: type(b)
        <class 'int'>
    """
    return (int(q.numerator()), int(q.denominator()))


def py_to_rational(t):
    r"""
    Converts a pair of python integers (a,b) into the rational number a/b.

    EXAMPLES::

        sage: from admcycles.file_cache import py_to_rational
        sage: q = py_to_rational((1, 2))
        sage: q
        1/2
        sage: type(q)
        <class 'sage.rings.rational.Rational'>
    """
    return QQ(t[0]) / QQ(t[1])


def rational_vectors_to_py(vs):
    r"""
    Converts a list of vectors over QQ into a list of tuples of pairs of python integers.

    EXAMPLES::

        sage: from admcycles.file_cache import rational_vectors_to_py
        sage: v = rational_vectors_to_py([vector(QQ, [1/2, 1])])
        sage: v
        [((1, 2), (1, 1))]
    """
    return [tuple(rational_to_py(a) for a in v) for v in vs]


def py_to_rational_vectors(vs):
    r"""
    Converts a list of tuples of pairs of python integers into a list of sparse vectors over QQ.

    EXAMPLES::

        sage: from admcycles.file_cache import py_to_rational_vectors
        sage: v = py_to_rational_vectors([((1, 2), (1, 1))])
        sage: v
        [(1/2, 1)]
        sage: type(v[0])
        <class 'sage.modules.free_module_element.FreeModuleElement_generic_sparse'>
    """
    return [vector(QQ, (py_to_rational(q) for q in v), sparse=True) for v in vs]
