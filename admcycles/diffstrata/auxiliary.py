from collections import defaultdict
import itertools

# pylint does not know sage
from sage.misc.flatten import flatten  # pylint: disable=import-error
from sage.misc.cachefunc import cached_function  # pylint: disable=import-error
from sage.rings.number_field.number_field import CyclotomicField  # pylint: disable=import-error

import admcycles.diffstrata.levelgraph
import admcycles.diffstrata.embeddedlevelgraph
import admcycles.diffstrata.sig

#################################################################
#################################################################
#   Auxiliary functions:
#################################################################
#################################################################


def unite_embedded_graphs(X, gen_LGs):
    r"""
    Create a (disconnected) EmbeddedLevelGraph from a tuple of tuples that generate EmbeddedLevelGraphs.

    (The name is slightly misleading, but of course it does not make sense to actually unite two complete
    EmbeddedLevelGraphs, as the checks would (and do!) throw errors otherwise! Therefore, this essentially
    takes the data of a LevelGraph embedded into each connected componenent of a GeneralisedStratum and
    returns an EmbeddedLevelGraph on the product.)

    This should be used on (products) of BICs in generalised strata.

    INPUT:

    gen_LGs: tuple
    A tuple of tuples that generate EmbeddedLevelGraphs.
    More precisely, each tuple is of the form:

    * X (GeneralisedStratum): Enveloping stratum (should be the same for all tuples!)
    * LG (LevelGraph): Underlying LevelGraph
    * dmp (dict): (partial) dictionary of marked points
    * dlevels (dict): (partial) dictionary of levels

    OUTPUT:

    The (disconnected) LevelGraph obtained from the input with the legs
    renumbered (continuously, starting with 1), and the levels numbered
    according to the embedding.
    """
    if len(gen_LGs) == 1 and gen_LGs[0][0].cover_of_k is not None:
        newLG, newdmp, newdlevels = gen_LGs[0]
        assert newLG.cover_of_k == X.k
    else:
        newgenera = []
        newlevels = []
        newlegs = []
        newpoleorders = {}
        newedges = []
        newdmp = {}
        newdlevels = {}
        newdeck = {}
        max_leg_number = 0
        new_cover_of_k = X.k

        for emb_g in gen_LGs:
            # Unpack tuple:
            LG, dmp, dlevels = emb_g
            if LG.deck is None:
                newdeck = None
                new_cover_of_k = None
            # the genera are just appended
            newgenera += LG.genera
            # same for the levels, but while we're at it, we might just as well
            # replace them by their embedding (then newdlevels will be trivial)
            # and these should be consistens for all graphs in the tuple.
            # Thus, newdlevels will be the identity.
            newlevels += [dlevels[l] for l in LG.levels]
            # the legs will have to be renumbered
            leg_dict = {}  # old number -> new number
            legs = 0
            for i, l in enumerate(flatten(LG.legs)):
                newlegnumber = max_leg_number + i + 1
                leg_dict[l] = newlegnumber
                # while we're at it, we add the pole orders:
                newpoleorders[newlegnumber] = LG.poleorders[l]
                # For the dictionary of marked points (for the embedding), we
                # must distinguish if this is a marked point or a half-edge.
                # Marked points are simply the ones for which we have a key
                # in dmp :-)
                try:
                    newdmp[newlegnumber] = dmp[l]
                except KeyError:
                    pass
                legs += 1
            max_leg_number += legs
            # append (nested) list of legs:
            newlegs += [[leg_dict[l] for l in comp] for comp in LG.legs]
            # finally, the edges are renumbered accordingly:
            newedges += [(leg_dict[e[0]], leg_dict[e[1]]) for e in LG.edges]
            # we collect the data for the deck transformation:
            if newdeck is not None:
                for l in itertools.chain(*LG.legs):
                    newdeck[leg_dict[l]] = leg_dict[LG.deck[l]]
        # the levels are already numbered according to the embedding dict
        if newdeck is None:
            newdeck = "UNINIT"
        newdlevels = {l: l for l in newlevels}
        newLG = admcycles.diffstrata.levelgraph.LevelGraph(
            newgenera, newlegs, newedges, newpoleorders, newlevels, 1, new_cover_of_k, newdeck
        )
    return admcycles.diffstrata.embeddedlevelgraph.EmbeddedLevelGraph(
        X, newLG, newdmp, newdlevels
    )


def prod_of_level_graph(X, LG, part_embs):
    max_leg = max(itertools.chain(*LG.legs))
    LGs = [LG] + [LG.rename_legs_by(max_leg * n) for n in range(1, len(part_embs))]
    new_genera = flatten([LG.genera for LG in LGs])
    new_legs = flatten([LG.legs for LG in LGs], max_level=1)
    new_edges = flatten([LG.edges for LG in LGs], max_level=1)
    new_poleorders = dict(itertools.chain(*[LG.poleorders.items() for LG in LGs]))
    new_levels = flatten([LG.levels for LG in LGs])
    # We need to build the new deck transformation: If n = len(part_mebs), then
    # the new transformation sends the legs of the first n-1 components
    # to the corresponding leg of the next component. The images of the legs
    # of the n-th component lie on the first component and are determined by
    # LG.deck.
    new_deck = {}
    for l in itertools.chain(*LG.legs):
        for i in range(len(part_embs) - 1):
            new_deck[l + max_leg * i] = l + max_leg * (i + 1)
        new_deck[l + max_leg * (len(part_embs) - 1)] = LG.deck[l]
    new_LG = admcycles.diffstrata.levelgraph.LevelGraph(new_genera, new_legs, new_edges, new_poleorders, new_levels, 1, X.k, new_deck)
    part_embs_corrected = {l + i * max_leg: p for i, pe in enumerate(part_embs) for l, p in pe.items()}
    emb = part_embs_corrected.copy()
    for l, p in part_embs_corrected.items():
        pp = X.deck[p]
        ll = new_LG.deck[l]
        while ll != l:
            emb[ll] = pp
            pp = X.deck[pp]
            ll = new_LG.deck[ll]
        assert pp == p
    return new_LG, emb


def sort_with_dict(l):
    r"""
    Sort a list and provide a dictionary relating old and new indices.

    If x had index i in l, then x has index sorted_dict[i] in the sorted l.

    Args:
        l (list): List to be sorted.

    Returns:
        tuple: A tuple consisting of:
            list: The sorted list l.
            dict: A dictionary old index -> new index.
    """
    sorted_list = []
    sorted_dict = {}
    for i, (j, v) in enumerate(sorted(enumerate(l), key=lambda w: w[1])):
        sorted_list.append(v)
        sorted_dict[j] = i
    return sorted_list, sorted_dict


def get_squished_level(deg_ep, ep):
    r"""
    Get the (relative) level number of the level squished in ep.

    This is the index of the corresponding BIC in the profile.

    Args:
        deg_ep (tuple): enhanced profile
        ep (tuple): enhanced profile

    Raises:
        RuntimeError: raised if deg_ep is not a degeneration of ep

    Returns:
        int: relative level number
    """
    deg_p = deg_ep[0]
    p = set(ep[0])
    for i, b in enumerate(deg_p):
        if b not in p:
            break
    else:
        raise RuntimeError("%r is not a degeneration of %r!" % (deg_ep, p))
    return i


def get_orbits(d):
    r"""
    Computes the orbits of a permutation given by the dictionary d.

    EXAMPLES:

        sage: from admcycles.diffstrata.auxiliary import get_orbits
        sage: get_orbits({1: 1, 2: 3, 3: 2})
        [[1], [2, 3]]
    """
    orbits = []
    seen = set()
    for l in d:
        if l in seen:
            continue
        o = [l]
        seen.add(l)
        ll = d[l]
        while l != ll:
            o.append(ll)
            seen.add(ll)
            ll = d[ll]
        orbits.append(o)
    return orbits

#################################################################
#################################################################
#    Auxiliary functions for caching:
#################################################################
#################################################################


def hash_AG(leg_dict, enh_profile):
    r"""
    The hash of an AdditiveGenerator, built from the psis and the enhanced profile.

    The hash-tuple is (leg-tuple,profile,index), where profile is
    changed to a tuple and leg-tuple is a nested tuple consisting of
    tuples (leg,exponent) (or None).

    Args:
        leg_dict (dict): dictioary for psi powers (leg -> exponent)
        enh_profile (tuple): enhanced profile

    Returns:
        tuple: nested tuple
    """
    if leg_dict is None:
        leg_hash = ()
    else:
        leg_hash = tuple(sorted(leg_dict.items()))
    return (leg_hash, tuple(enh_profile[0]), enh_profile[1])


def adm_key(k, sig, psis):
    r"""
    The hash of a psi monomial on a connected stratum without residue conditions.

    This is used for caching the values computed using admcycles (using
    GeneralisedStratum.adm_evaluate)

    The signature is sorted, the psis are renumbered accordingly and also
    sorted (with the aim of computing as few duplicates as possible).

    Args:
        sig (tuple): signature tuple
        psis (dict): psi dictionary

    Returns:
        tuple: nested tuple
    """
    sorted_psis = {}
    sorted_sig = []
    psi_by_order = defaultdict(list)
    # sort signature and relabel psis accordingly:
    # NOTE: Psis are labelled 'mathematically', i.e. 1,...,len(sig)
    for new_i, (old_i, order) in enumerate(
            sorted(enumerate(sig), key=lambda k: k[1])):
        psi_new_i = new_i + 1
        psi_old_i = old_i + 1
        sorted_sig.append(order)
        if psi_old_i in psis:
            assert psi_new_i not in sorted_psis
            psi_exp = psis[psi_old_i]
            sorted_psis[psi_new_i] = psi_exp
            psi_by_order[order].append(psi_exp)
    # sort psis for points of same order:
    ordered_sorted_psis = {}
    i = 0
    assert len(sig) == len(sorted_sig)
    while i < len(sig):
        order = sorted_sig[i]
        for j, psi_exp in enumerate(sorted(psi_by_order[order])):
            assert sorted_sig[i + j] == order
            ordered_sorted_psis[i + j + 1] = psi_exp
        while i < len(sig) and sorted_sig[i] == order:
            i += 1
    return (k, tuple(sorted_sig), tuple(sorted(ordered_sorted_psis.items())))


def primitive_fundamental_class(sig):
    r"""
    A wrapper for `admcycles.stratarecursion.Strataclass` that computes the
    fundamental class of the stratum of primitive k-differentials given by sig.
    """
    return admcycles.stratarecursion.Strataclass(sig.g, sig.k, sig.sig, virt=(sig.k != 1), primitive=True)


@cached_function
def cyclotomic_field_cache(k):
    r"""
    A simple cache for the cyclotomic fields of order k and there generators.
    """
    K = CyclotomicField(k)
    zeta = K.gen()
    return (K, zeta)


class ConsumeListIter:
    r"""
    A wrapper that takes a list as argument and removes the last element
    while iterating over it. This is usefull in the bic generation, as we
    iterate over large lists of graphs and usual iteration would esentially
    used two times as much memory at the end of the iteration.
    """
    def __init__(self, lst):
        self.lst = lst

    def __iter__(self):
        return self

    def __next__(self):
        if self.lst:
            return self.lst.pop()
        else:
            raise StopIteration
