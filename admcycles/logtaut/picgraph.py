# from admcycles.admcycles import *
import numbers

# from sage.all_cmdline import *   # import sage library
import sage
from sage.misc.cachefunc import cached_function
from sage.misc.misc_c import prod
from sage.graphs.digraph import DiGraph

from sage.structure.unique_representation import UniqueRepresentation
from sage.structure.richcmp import op_EQ, op_NE
from sage.structure.parent import Parent
from sage.structure.element import ModuleElement, parent
from sage.geometry.fan import Cone_of_fan

from sage.categories.functor import Functor
from sage.categories.pushout import ConstructionFunctor
from sage.categories.algebras import Algebras
from sage.categories.rings import Rings
from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.matrix.constructor import matrix, Matrix
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.combinat.integer_vector import IntegerVectors
from sage.sets.set import Set
from sage.functions.other import floor, ceil
from sage.categories.homset import Hom
from sage.geometry.fan import Fan
from sage.combinat.subset import Subsets
from sage.rings.integer import Integer
from sage.categories.vector_spaces import VectorSpaces
from sage.geometry.cone import Cone
from sage.geometry.fan_morphism import FanMorphism
from sage.modules.free_module_element import vector
from sage.functions.other import factorial, binomial
from sage.misc.prandom import random, choice
from sage.geometry.toric_lattice import ToricLattice

from collections import defaultdict
from copy import copy


from admcycles import StableGraph, TautologicalRing, list_strata
from admcycles.admcycles import degeneration_graph, deggrfind, dicv_reconstruct
from admcycles.double_ramification_cycle import DR_coeff_new

import itertools


def powerset(iterable):
    r"""
    Returns the power set of iterable, as a list of lists.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import powerset
        sage: powerset([1,2,3])
        [[], [1], [2], [3], [1, 2], [1, 3], [2, 3], [1, 2, 3]]
    """
    s = list(iterable)
    return [list(a) for a in itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s) + 1))]


class PicGraph(StableGraph):
    r"""
    A PicGraph is a (pre)stable graph together with an assignment multideg of
    integers to its vertices. It describes a boundary stratum of the universal
    Picard stack Pic_{g,n}, where multideg describes the degree of the line bundle
    on the components of the prestable curve.

    EXAMPLES::

        sage: from admcycles.logtaut import PicGraph
        sage: G = PicGraph([1,3],[[1,2],[3]],[(2,3)],[7,-1])
        sage: G.total_degree()
        6
    """
    def __init__(self, genera, legs, edges, multideg, mutable=False, check=True):
        r"""
        INPUT:

        - ``genera`` -- (list) List of genera of the vertices of length m.

        - ``legs`` -- (list) List of length m, where ith entry is list of legs
          attached to vertex i.

        - ``edges`` -- (list) List of edges of the graph. Each edge is a 2-tuple of legs.

        - ``mutable`` - (boolean, default to ``False``) whether this stable graph
          should be mutable

        - ``check`` - (boolean, default to ``True``) whether some additional sanity
          checks are performed on input

        - ``multideg`` is a function from vertices to integers, represented as a list of integers
        """
        StableGraph.__init__(self, genera, legs, edges,
                             mutable=False, check=True)
        self.multideg = multideg
        if check:
            if len(genera) != len(multideg):
                raise ValueError(
                    "multideg should be a list of integers of the same length as the list of genera")

    def __repr__(self):
        r"""
        Returns string representation of PicGraph.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PGs = PicGraph([1,2,1],[[1,2],[3,4],[5]],[(2,3),(4,5)], [4,-1,-1])
            sage: PGs
            [(1, 4), (2, -1), (1, -1)] [[1, 2], [3, 4], [5]] [(2, 3), (4, 5)]
        """
        return repr([(g, self.multideg[i]) for i, g in enumerate(self._genera)]) + ' ' + repr(self._legs) + ' ' + repr(self._edges)

    def total_degree(self):
        r"""
        Computes the total degree of the line bundle described by the PicGraph.

        EXAMPLES::

            sage: from admcycles.logtaut import PicGraph
            sage: PG = PicGraph([1,2],[[1,2],[3]],[(2,3)], [4,-2])
            sage: PG.total_degree()
            2
        """
        return sum(self.multideg)

    # here V is a list of integers between 0 and the length of the list of vertices -1
    def subset_deg(self, V):
        r"""
        Computes the total degree of the line bundle supported on the vertices V of the PicGraph.

        INPUT:

        - ``V`` -- (list) List of vertices of the PicGraph.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PG = PicGraph([1,2,1],[[1,2],[3,4],[5]],[(2,3),(4,5)], [4,-1,-1])
            sage: PG.subset_deg([0,2])
            3
        """
        m = self.multideg
        return sum(m[i] for i in V)

    # the constant from section 4 of Abreu-Pacini, The Universal...
    def subset_beta(self, V):
        r"""
        Computes the constant from section 4 of Abreu-Pacini, The Universal...
        for V a subset of the vertices of the PicGraph.

        INPUT:

        - ``V`` -- (list) List of vertices of the PicGraph.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PG = PicGraph([1,2,1],[[1,2],[3,4],[5]],[(2,3),(4,5)], [4,-1,-1])
            sage: PG.subset_beta([0])
            9/2
        """
        return self.subset_deg(V) + ZZ.one() / 2 * self.subset_valence(V)

    # checks whether quasi-stable, with respect to the vertex v
    def is_quasistable(self, v, paranoid=False):
        r"""
        Check whether the the PicGraph is quasi-stable with respect to the stability parameter v.

        INPUT:

        - ``v`` -- Stability parameter. Might be an integer (i.e. a vertex) with respect
                   to which stability is checked in the sense of Esteves/Abreu-Pacini. Or might be
                   a Kass-Pagani stability condition.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PG = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [1,-1])
            sage: PG.is_quasistable(0)
            True
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: PGs = PicGraph([0,1],[[1,2,3],[4]],[(3,4)], [4,-4])
            sage: PGs.is_quasistable(phi)
            False
            sage: PGs = PicGraph([0,1],[[1,2,3],[4]],[(3,4)], [3,-3])
            sage: PGs.is_quasistable(phi)
            True
            sage: phi = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: PGs = PicGraph([0,0],[[1,2,3,4],[5,6]],[(3,5),(4,6)], [4,-4])
            sage: PGs.is_quasistable(phi)
            False
            sage: PGs = PicGraph([0,0],[[1,2,3,4],[5,6]],[(3,5),(4,6)], [1,-1])
            sage: PGs.is_quasistable(phi)
            True
            """
        if isinstance(v, (int, Integer)):
            vertices = list(range(len(self.genera())))
            other_vertices = [x for x in vertices if not x == v]
            strict_ineq_subsets = [V + [v] for V in powerset(other_vertices)]
            for V in strict_ineq_subsets:
                # print(V)
                # print(self.subset_beta(V))
                if self.subset_beta(V) <= 0 and len(V) != len(self.genera()):
                    return False
                Vc = [i for i in vertices if i not in V]
                # print(Vc)
                # print(self.subset_beta(Vc))
                if self.subset_beta(Vc) < 0:
                    return False
            return True
        if isinstance(v, GraphStabilityCondition):
            vnum = self.num_verts()
            phidic = {w: v.weight(self, w) for w in range(vnum)}
            # for numI0 in range(1,vnum):
            # print("numI0", numI0)
            for I0 in Subsets(range(vnum)):
                if len(I0) == vnum or len(I0) == 0:
                    continue
                # print(I0, self.subset_deg(I0), self.subset_valence(I0))
                # Implement formula from Definition 4.1 in [KassPagani - The stability space of compactified universal Jacobians]
                s = self.subset_deg(I0) - sum(phidic[w] for w in I0)
                # print(I0,abs(s) - self.subset_valence(I0)/ZZ(2))
                if abs(s) > self.subset_valence(I0) / ZZ(2):
                    return False
            if paranoid:
                MM = self.list_markings()
                for vertn in range(1, vnum):
                    if self.genera()[vertn] == 0:
                        HH = self.legs()[vertn]
                        if len(HH) == 2:
                            if HH[0] not in MM and HH[1] not in MM:
                                if self.multideg[vertn] not in [0, -1]:
                                    return False
            return True

    def laplacian(self):
        r"""
        Computes the laplacian matrix of a PicGraph. This is a (symmetric) square
        integer matrix of size the number of vertices.
        The off-diagonal entries count the edges between two vertices.
        The diagonal entries make the row (and column) sums zero.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PGs = PicGraph([1,2,1],[[1,2],[3,4],[5]],[(2,3),(4,5)], [4,-1,-1])
            sage: PGs.laplacian()
            [-1  1  0]
            [ 1 -2  1]
            [ 0  1 -1]
        """
        rows = []
        vertexlist = list(range(len(self.genera())))
        for v in vertexlist:
            this_row = []
            for u in vertexlist:
                if u != v:
                    this_row.append(len(self.edges_between(u, v)))
                else:
                    nn = 0
                    for u in vertexlist:
                        if u != v:
                            nn = nn - len(self.edges_between(u, v))
                    this_row.append(nn)
            rows.append(this_row)
        return matrix(rows)

    def is_principal(self, certificate=False):
        r"""
        Check whether the multidegree on the PicGraph is the divisor of a
        piecewise-linear function (for all edge-lengths being 1).
        If certificate=True, also output such a function.

        INPUT:

        EXAMPLES::

            sage: from admcycles.logtaut import PicGraph
            sage: PG = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [1,-1])
            sage: PG.is_principal()
            False
            sage: G = PicGraph([1,3],[[1,2],[3]],[(2,3)],[7,-1])
            sage: G.is_principal()
            False
            sage: H = PicGraph([1,1],[[1,2],[3,4]],[(1,3),(2,4)],[5,-5])
            sage: H.is_principal(certificate=True)
            False
            sage: H = PicGraph([1,1],[[1,2],[3,4]],[(1,3),(2,4)],[4,-4])
            sage: H.is_principal(certificate=True)
            (
                  [0]
            True, [2]
            )
        """
        if self.total_degree() != 0:
            return False
        L = self.laplacian()
        M = L.pseudoinverse()
        mm = M * matrix(self.multideg).transpose()
        mm = mm - matrix([mm[0] for i in range(mm.dimensions()[0])])
        if mm.denominator() == 1:
            return (True, mm) if certificate else True
        return False

    def pullback_to_subdivision(self, EE):
        r"""
        Given a subset of the edges, return the PicGraph obtained by inserting
        a vertex in the middle of each of the given edge, with degree 0.

        INPUT:

        - ``EE`` -- A list which is a subset of the list of edges (as ordered pairs of legs).

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PG = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [1,-1])
            sage: PG.pullback_to_subdivision([(1,4)])
            [(0, 1), (0, -1), (0, 0)] [[1, 2, 3], [4, 5, 6, 7], [8, 9]] [(2, 5), (3, 6), (1, 8), (9, 4)]
        """
        G = assign_divisions(self, EE)
        GG = G.expanded_graph(newdeg=0)
        return GG

    def quasistable_representative(self, phi):
        r"""
        Finds the unique quasi-stable representative of the PicGraph with
        respect to the stability condition phi.

        INPUT:

        - ``v`` -- condition with respect to which stability is checked

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):0.1, (2,):-0.1})
            sage: PGs = PicGraph([0,0],[[1,3,4],[2, 5,6]],[(3,5),(4,6)], [0, 0])
            sage: PGs.quasistable_representative(phi)
            [(0, 0), (0, 0)] [[1, 3, 4], [2, 5, 6]] [(3, 5), (4, 6)] []
            sage: PGs = PicGraph([0,0],[[1,3,4],[2, 5,6]],[(3,5),(4,6)], [5, -5])
            sage: PGs.quasistable_representative(phi)
            [(0, 1), (0, -1)] [[1, 3, 4], [2, 5, 6]] [(3, 5), (4, 6)] []
           """
        # old doctest:
        # sage: PG = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [3,-3])
        # sage: PG.quasistable_representative(0)
        # [(0, 0), (0, 0)] [[1, 2, 3], [4, 5, 6, 7]] [(1, 4), (2, 5), (3, 6)] []
        QSlist = quasi_stable_multidegree_list(self, phi)
        for G in QSlist:
            if are_linearly_equivalent(G.expanded_graph(), self):
                return G

    def list_relevant_flows(self, full=False):
        r"""
        Outputs a list of the Suzumura consistent flows with the given multidegree.

        NOTE:
        The current implementation seems only to include flows which are non-zero
        on every edge (check). If so, should expand this a bit.

        EXAMPLES::
            sage: from admcycles.logtaut import *
            sage: PG = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [-4,4])
            sage: PG.list_relevant_flows()
            [[0, 0] [[1, 2, 3], [4, 5, 6, 7]] [(1, 4), (2, 5), (3, 6)] {4: 2, 1: -2, 5: 1, 2: -1, 6: 1, 3: -1, 7: 0},
            [0, 0] [[1, 2, 3], [4, 5, 6, 7]] [(1, 4), (2, 5), (3, 6)] {4: 1, 1: -1, 5: 2, 2: -2, 6: 1, 3: -1, 7: 0},
            [0, 0] [[1, 2, 3], [4, 5, 6, 7]] [(1, 4), (2, 5), (3, 6)] {4: 1, 1: -1, 5: 1, 2: -1, 6: 2, 3: -2, 7: 0}]
            sage: PG = PicGraph([0,0,0],[[1,2],[3,4],[5,6]],[(2,3),(4,5),(6,1)], [2,-1,-1])
            sage: PG.list_relevant_flows() # has zero flow on some edge
            [[0, 0, 0] [[1, 2], [3, 4], [5, 6]] [(2, 3), (4, 5), (6, 1)] {1: 1, 6: -1, 2: 1, 3: -1, 4: 0, 5: 0}]
            """
        # TODO: make this function output also flows with some zeros in.
        if not full:
            flowlist = acyclic_flows(self.multideg, self.legs(), self.edges())
        else:
            flowlist = weak_acyclic_flows(
                self.multideg, self.legs(), self.edges())
        return [FlowOnGraph(self.genera(), self.legs(), self.edges(), flowdict) for flowdict in flowlist]
        # BB = self.cycle_basis()
        # first = self.flow_solve(self.multideg)
        # output = [first]
        # m = max([abs(i) for i in first])
        # MM = range(-m,m)
        # d = dim(self.cycle_space())
        # e = len(self.edges())
        # for v in itertools.product(MM, repeat=d):
        #     newvect = [first[i] + sum([v[f]*BB[f][i] for f in range(d)]) for i in range(e)]
        #     output.append(newvect)
        # return [flow_from_list(self, v) for v in output]

    def incomplete_fan(self, flows=False, full=False):
        r"""
        Outputs the incomplete fan for this PicGraph.

        EXAMPLES::
            sage: from admcycles.logtaut import *
            sage: G1 = PicGraph([0,0],[[1,2,3],[4,5,6]],[(1,4),(2,5),(3,6)], [4,-4])
            sage: FFF = G1.incomplete_fan()
            sage: sorted(FFF.rays())
            [N(1, 2, 2), N(2, 1, 2), N(2, 2, 1)]
        """
        e = self.num_edges()
        NN = ToricLattice(e)
        mylist = self.list_relevant_flows(full=full)
        cones_and_flows = [[FF, FF.cone()] for FF in mylist]
        cones = [X[1] for X in cones_and_flows]
        Dfan = Fan(cones, discard_faces=True, lattice=NN)
        if not flows:
            return Dfan
        if not cones_and_flows:
            return Dfan, {}
        mcones = Dfan.generating_cones()
        mcones_and_flows = {}
        for C in mcones:
            Cflows = [a[0] for a in cones_and_flows if a[1].is_equivalent(C)]
            assert len(Cflows) == 1
            mcones_and_flows[C] = Cflows[0]
        if flows:
            return Dfan, mcones_and_flows
        else:
            return Dfan

    def complete_fan(self, v, flows=False):
        r"""
        Returns complete fan associated to a stability condition.

        INPUT:

        - ``v`` -- Stability parameter.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: V = GraphStabilitySpace(2,2)
            sage: phi = V(0,{(0,(1,2)):1, (1,(1,2)):0.1, (1,(1,)):0.11, (1,):0.15, (2,):0.1})
            sage: PG = PicGraph([0,0],[[1,2,3],[4,5,6]],[(1,4),(2,5),(3,6)], [4,-4])
            sage: FF = PG.complete_fan(phi)
            sage: sorted(FF.rays())
            [N(0, 0, 1),
             N(0, 1, 0),
             N(1, 0, 0),
             N(1, 1, 1),
             N(1, 1, 2),
             N(1, 2, 1),
             N(1, 2, 2),
             N(1, 3, 3),
             N(2, 1, 1),
             N(2, 1, 2),
             N(2, 2, 1),
             N(3, 1, 3),
             N(3, 3, 1)]
        """
        g, n = self.g(), self.n()
        smalllattice = self.incomplete_fan().lattice()
        QSlist = quasi_stable_multidegree_list(self, v)
        mycones = []
        myflows = {}
        for PPG in QSlist:
            oldE = PPG.edges()
            EPPGqs = PPG.expanded_graph(newdeg=-1)
            newE = EPPGqs.edges()
            # This graph EPPG is equipped with the quasi-stable degree. Now we need to also pull back the degree from the original graph, then take the difference.
            EPPG_other_deg = self.pullback_to_subdivision(newE)
            EPPG = PicGraph(EPPGqs.genera(), EPPGqs.legs(), EPPGqs.edges(), [
                            EPPGqs.multideg[i] - EPPG_other_deg.multideg[i] for i in range(len(EPPGqs.genera()))])
            if not flows:
                newfan = EPPG.incomplete_fan(flows=False)
            else:
                newfan, flow_dict = EPPG.incomplete_fan(flows=True)
            biglattice = newfan.lattice()

            def oldedgemap(x):
                i = next(a for a in x.dict())
                e = newE[i]
                for j in range(len(oldE)):
                    ee = oldE[j]
                    if e[0] == ee[0] or e[1] == ee[1] or e[0] == ee[1] or e[1] == ee[0]:
                        return smalllattice.basis()[j]

            HH = Hom(biglattice, smalllattice)
            latticemap = HH(oldedgemap)
            FM = FanMorphism(latticemap, newfan)
            pushed_fan = FM.codomain_fan()  # Not sure this step is justified.
            if flows:
                # [a for b in newfan.cones() for a in b]:
                for CC, fCC in flow_dict.items():
                    imCC = FM.image_cone(CC)
                    myflows[imCC] = [fCC, PPG.gen_multideg]
            mycones = mycones + list(pushed_fan.cones())
            # print([c.rays() for c in pushed_fan.generating_cones()])#[item for sublist in pushed_fan for item in sublist])
            # still need to take image; make them live in the same place!
        resultfan = Fan([a for b in mycones for a in b if a.dim() == 3 * g - 3 + n], discard_faces=True, lattice=smalllattice)
        if flows:
            return resultfan, myflows
        return resultfan

    def convex_pl_function(self, v):
        # CF = self.complete_fan(v)
        # maximal_cones = CF.generating_cones()
        # CFrays = FF.rays()
        return NotImplementedError

    # def values_of_second_PL_function(self, v, vert0=0):
        # CF, flow_dict = self.complete_fan(v, flows=True)
        # fdk = [a for a in flow_dict]
        # mylattice = CF.lattice()
        # myrays = CF.rays()
        # value_dict = {}
        # for RR in myrays:
        #     RC = Cone([RR], lattice=mylattice)
        #     if RC in fdk:  # should always be True, but problem with Suzumra implementation.
        #         myflow = flow_dict[RC][0]
        #         stable_multideg = flow_dict[RC][1][0]
        #         edge_leng_dict = {}
        #         for i in range(len(self.edges())):
        #             edge_leng_dict[self.edges()[i]] = RR[i]
        #         alpha = myflow.integrate(vert0, edge_leng_dict)
        #         value_dict[RC] = sum(
        #             [alpha[i] * (self.multideg[i] + stable_multideg[i]) for i in range(len(self.genera()))])
        # return value_dict

    # def second_PL_function(self, v, vert0=0):
        # mydict = self.values_of_second_PL_function(v, vert0=vert0)
        # mykeys = mydict
        # myrays = [[c.rays()[0].coefficients(), mydict[c]] for c in mykeys]
        # return PL_from_fan_and_ray_values(self.complete_fan(v), myrays)

    def second_PL_function(self, v, vert0=0):
        r"""
        Compute function f_2 from formula of logDR via maximal cones.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import *
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):0.1, (2,):-0.1})
            sage: PGs = PicGraph([0,0],[[1,3,4],[2, 5,6]],[(3,5),(4,6)], [3, -3])
            sage: PGs.second_PL_function(phi)
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 1), N(1, 2)) : -2*x0 - 2*x1
            (N(1, 0), N(2, 1)) : -6*x1
            (N(0, 1), N(1, 2)) : -6*x0
            (N(1, 1), N(2, 1)) : -2*x0 - 2*x1
        """
        g = self.g()
        n = self.n()
        CF, flow_dict = self.complete_fan(v, flows=True)
        # need to get our hands on list of
        # * maximal cones C of complete fan
        # * extended graph EG and flow FF on EG associated to C
        polydict = {}
        for C in (C for C in flow_dict if C.dim() == 3 * g - 3 + n):
            FOG = flow_dict[C][0]
            EG = FOG
            FF = FOG.flow

            # Polynomial ring on unexpanded graph
            S = PolynomialRing(QQ, self.num_edges(), 'x')
            x = S.gens()
            # Polynomial ring on expanded graph
            T = PolynomialRing(S, EG.num_edges(), 'y')
            y = T.gens()

            # write down the piecewise polynomial function on EG
            EGweights = flow_dict[C][1][0] + [-1 for i in flow_dict[C][1][1]]
            # intlegweights = [sum(FF[l] for l in LL if l > n)
            #                  for LL in EG.legs()]
            legweights = [sum(FF[l] for l in LL) for LL in EG.legs()]
            edge_leng_dict = dict(zip(EG.edges(), y))
            alpha = EG.integrate(vert0, edge_leng_dict)
            EGpoly = sum([alpha[i] * (-2 * EGweights[i] + legweights[i])
                         for i in range(EG.num_verts())], T.zero())
            # now we substitute the appropriate linear combinations of the variables x_j
            # of self into the variables y_i of the expanded graph
            M = subdivided_edges_to_lin_combs_of_edges(self, EG, FF)
            subsdict = {yi: sum(M[i, j] * xj for j, xj in enumerate(x))
                        for i, yi in enumerate(y)}
            result = S(EGpoly.subs(subsdict))
            polydict[C] = result
        R = PiecewisePolynomialRing(CF)
        return R(polydict)

    def first_PP_function(self, dmax, v, vert0=0):
        r"""
        Compute function f_1 from formula of logDR up to degree dmax.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import *
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):0.1, (2,):-0.1})
            sage: PGs = PicGraph([0,0],[[1,3,4],[2, 5,6]],[(3,5),(4,6)], [3, -3])
            sage: PGs.first_PP_function(2,phi)
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 1), N(1, 2)) : -7/80*x0^2 + 1/5*x0*x1 - 7/80*x1^2 - 1/12*x0 - 1/12*x1 + 1
            (N(1, 0), N(2, 1)) : -1/240*x0^2 + 7/60*x0*x1 - 61/240*x1^2 - 1/12*x0 - 1/12*x1 + 1
            (N(0, 1), N(1, 2)) : -61/240*x0^2 + 7/60*x0*x1 - 1/240*x1^2 - 1/12*x0 - 1/12*x1 + 1
            (N(1, 1), N(2, 1)) : -7/80*x0^2 + 1/5*x0*x1 - 7/80*x1^2 - 1/12*x0 - 1/12*x1 + 1
        """
        g = self.g()
        n = self.n()
        CF, flow_dict = self.complete_fan(v, flows=True)
        # need to get our hands on list of
        # * maximal cones C of complete fan
        # * extended graph EG and flow FF on EG associated to C
        polydict = {}
        for C in (C for C in flow_dict if C.dim() == 3 * g - 3 + n):
            FOG = flow_dict[C][0]
            EG = FOG
            FF = FOG.flow

            # Polynomial ring on unexpanded graph
            S = PolynomialRing(QQ, self.num_edges(), 'x')
            x = S.gens()
            # Polynomial ring on expanded graph
            T = PolynomialRing(S, EG.num_edges(), 'y')
            y = T.gens()

            # write down the piecewise polynomial function on EG
            EGpoly = T.zero()
            numEG = EG.num_edges()
            EGweights = flow_dict[C][1][0] + [-1 for i in flow_dict[C][1][1]]
            for d in range(dmax + 1):
                for W in IntegerVectors(d, numEG):
                    # want to compute the coefficient of y^W
                    # for this: need graph EGc obtained from EG by contracting edges with weight 0 in W
                    if 0 in W:
                        EGc = EG.copy()
                        diccv = EGc.contract_edge(
                            [e for i, e in enumerate(EG.edges()) if W[i] == 0], adddata=True)
                        Wc = [w for w in W if w != 0]
                        given_weights = [sum(EGweights[w] for w in range(
                            EG.num_verts()) if diccv[w] == v) for v in range(EGc.num_verts())]
                    else:
                        EGc = EG
                        Wc = W
                        given_weights = EGweights
                    dat = (Wc, given_weights)
                    coe = EGc.automorphism_number() * DR_coeff_new(EGc, g, n,
                                                                   d, dat, directweights=True)
                    EGpoly += coe * T.monomial(*W)

            # now we substitute the appropriate linear combinations of the variables x_j
            # of self into the variables y_i of the expanded graph
            M = subdivided_edges_to_lin_combs_of_edges(self, EG, FF)
            subsdict = {yi: sum(M[i, j] * xj for j, xj in enumerate(x))
                        for i, yi in enumerate(y)}
            result = S(EGpoly.subs(subsdict))
            polydict[C] = result
        R = PiecewisePolynomialRing(CF)
        return R(polydict)

    # def normalised_f2(self, v, vert0=0):
        # asd = self.values_of_second_PL_function(v, vert0)
        # mylist = [[C.rays()[0], asd[C]] for C in [a for a in asd]]
        # # print(mylist)
        # newlist = []
        # for xx in mylist:
        #     coords = xx[0]
        #     ilen = sum(coords)**(-1)
        #     normalisedcoords = [a * ilen for a in coords]
        #     normalisedval = xx[1] * ilen
        #     newlist.append([normalisedcoords, normalisedval])
        # return newlist


def are_linearly_equivalent(G1, G2):
    r"""
    Check whether two PicGraphs are linearly equivalent.

    NOTE:
    It first checks that the underlying graphs are the same! If you want
    to be flexible about pullbacks, need a new function.

    INPUT:

    - ``G1`` -- (PicGraph) first PicGraph
    - ``G2`` -- (PicGraph) second PicGraph; must have same underlying graph as the first

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import PicGraph, are_linearly_equivalent
        sage: G1 = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [1,-1])
        sage: G2 = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [0,0])
        sage: G3 = PicGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [3,-3])
        sage: are_linearly_equivalent(G1,G2)
        False
        sage: are_linearly_equivalent(G2, G3)
        True
    """
    if G1.genera() != G2.genera() or G1.legs() != G2.legs() or G1.edges() != G2.edges():
        return False
    G = PicGraph(G1.genera(), G1.legs(), G1.edges(), [
                 G1.multideg[i] - G2.multideg[i] for i in range(len(G1.multideg))])
    return G.is_principal()

# I think there is a problem with the next function: it should require that the complement of the subdivided edges forms a connected graph (or something like that).


# these should correspond to points on the compactified jacobian a la Abreu-Pacini/(or Esteves or ....)
class PicPhiGraph(StableGraph):
    r"""
    A PicPhiGraph is a (pre)stable graph together with a multidegree (i.e. a
    function from vertices to integers) and a list of subdivided edges. These
    subdivided edges can be thought of as genus 0 components with degree -1.
    """
    def __init__(self, genera, legs, edges, gen_multideg, mutable=False, check=True):
        StableGraph.__init__(self, genera, legs, edges,
                             mutable=False, check=True)
        self.gen_multideg = gen_multideg
        """
        the generalised multidegree puts an integer weight on each vertex,
         and ALSO picks out some edges to get a weight -1 `in the middle'.
         It is given as a list containign two lists; the first ia a multidegree
         as for a graph in PicGraph, the second is a list of edges.
         """

    def __repr__(self):
        r"""
        Returns string representation of PicPhiGraph.

        EXAMPLES::

            sage: from admcycles.logtaut import PicPhiGraph
            sage: PPG = PicPhiGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [[1,-1], [(1,4), (2,5)]])
            sage: PPG
            [(0, 1), (0, -1)] [[1, 2, 3], [4, 5, 6, 7]] [(1, 4), (2, 5), (3, 6)] [(1, 4), (2, 5)]
        """
        return repr([(g, self.gen_multideg[0][i]) for i, g in enumerate(self._genera)]) + ' ' + repr(self._legs) + ' ' + repr(self._edges) + ' ' + repr(self.gen_multideg[1])

    def vertex_multideg(self):
        r"""
        Returns the list of multidegrees on (ordinary) vertices.

        EXAMPLES::

            sage: from admcycles.logtaut import PicPhiGraph
            sage: PPG = PicPhiGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [[1,-1], [(1,4), (2,5)]])
            sage: PPG.vertex_multideg()
            [1, -1]
        """
        return self.gen_multideg[0]

    def divided_edges(self):
        r"""
        Returns the list of divided edges.

        EXAMPLES::

            sage: from admcycles.logtaut import PicPhiGraph
            sage: PPG = PicPhiGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [[1,-1], [(1,4), (2,5)]])
            sage: PPG.divided_edges()
            [(1, 4), (2, 5)]
        """
        return self.gen_multideg[1]

    def complement_connected(self):
        r"""
        Checks if the graph obtained by cutting subdivided edges is still connected.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PPG = PicPhiGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [[1,-1], [(1,4), (2,5)]])
            sage: PPG.complement_connected()
            True
            sage: PPG = PicPhiGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [[1,-1], [(1,4), (2,5), (3,6)]])
            sage: PPG.complement_connected()
            False
        """
        cutedges = self.divided_edges()
        cutlegs = [e[i] for e in cutedges for i in [0, 1]]
        H = StableGraph(self.genera(), [[a for a in l if a not in cutlegs] for l in self.legs()], [
                        e for e in self.edges() if e not in cutedges])
        return H._graph()[0].is_connected()

    def contracted_graph(self):
        r"""
        Return PicGraph obtained by ignoring subdivided edges and taking given
        multidegree on (ordinary) vertices.

        EXAMPLES::

            sage: from admcycles.logtaut import PicPhiGraph
            sage: PPG = PicPhiGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [[1,-1], [(1,4), (2,5)]])
            sage: PPG.contracted_graph()
            [(0, 1), (0, -1)] [[1, 2, 3], [4, 5, 6, 7]] [(1, 4), (2, 5), (3, 6)]
        """
        return PicGraph(self.genera(), self.legs(), self.edges(), self.vertex_multideg())

    def expanded_graph(self, newdeg=-1):
        r"""
        Returns the PicGraph obtained by subdividing along the listed edges.
        The new vertices are given multidegree newdeg, which defaults to -1.
        The leg labels of the new graph are a superset of the leg labels of the old graph.
        The genera of the new vertices in the expanded graph are listed after those of the old graph.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: PPG = PicPhiGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], [[1,-1], [(1,4), (2,5)]])
            sage: PPG.expanded_graph()
            [(0, 1), (0, -1), (0, -1), (0, -1)] [[1, 2, 3], [4, 5, 6, 7], [8, 9], [10, 11]] [(3, 6), (1, 8), (9, 4), (2, 10), (11, 5)]
        """
        new_genera = self.genera() + [ZZ.zero() for a in self.divided_edges()
                                      ]  # add a vertex in the middle of each edge to be subdivided
        i = max(self.leglist()) + 1  # make sure we don't repeat leg labels
        new_legs = self.legs()
        # remove the edges that get divided
        new_edges = [e for e in self.edges() if e not in self.divided_edges()]
        for v in self.divided_edges():
            # add two new legs to each new vertex
            new_legs = new_legs + [[i, i + 1]]
            # join them up with the old half-edges
            new_edges = new_edges + [(v[0], i), (i + 1, v[1])]
            i = i + 2
        # note we give the new legs weight -1 instead of 1. This is different from Sam's convention, but same as in Abreu-Pacini. I don't think it matters really.
        mymultideg = self.vertex_multideg() + [ZZ(newdeg) for a in self.divided_edges()]
        return PicGraph(new_genera, new_legs, new_edges, mymultideg)

    def is_quasistable(self, phi):
        r"""
        Check if PicGraph is quasi-stable with respect to stability
        condition phi.

        EXAMPLES::

            sage: from admcycles.logtaut import GraphStabilitySpace, PicPhiGraph
            sage: V = GraphStabilitySpace(1, 2)
            sage: phi = V(0, {(1,2): 1/3, (1,): 2, (2,): -2})
            sage: PPG = PicPhiGraph([0,0],[[1,3,5],[2,4,6]],[(3,4), (5,6)], [[3,-2], [(3,4)]])
            sage: PPG.is_quasistable(phi)
            True
            sage: PPG2 = PicPhiGraph([0,0],[[1,3,5],[2,4,6]],[(3,4), (5,6)], [[4,-3], [(3,4)]])
            sage: PPG2.is_quasistable(phi)
            False
        """
        Egraph = self.expanded_graph()
        vnum = self.num_verts()
        newvnum = len(self.gen_multideg[1])
        phidic = {w: phi.weight(self, w) for w in range(vnum)}
        for vv in range(newvnum):
            phidic[vnum + vv] = 0
        for I0 in Subsets(range(vnum + newvnum)):
            I1 = I0.intersection(Set(range(vnum)))
            if len(I1) == vnum or len(I1) == 0:
                continue
            # Implement formula from Definition 4.1 in [KassPagani - The stability space of compactified universal Jacobians]
            s = Egraph.subset_deg(I0) - sum(phidic[w] for w in I0)
            if abs(s) > Egraph.subset_valence(I0) / ZZ(2):
                return False
        return True


def assign_multidegree(G, d):
    r"""
    Takes stable graph G and list d of degrees (length = number of
    vertices of G) and returns the associated PicGraph.

    EXAMPLES::

        sage: from admcycles import StableGraph
        sage: from admcycles.logtaut.picgraph import assign_multidegree
        sage: G = StableGraph([1,3], [[1], [2]], [(1,2)])
        sage: assign_multidegree(G, [8,-9])
        [(1, 8), (3, -9)] [[1], [2]] [(1, 2)]
    """
    return PicGraph(G.genera(), G.legs(), G.edges(), d)


StableGraph.assign_multidegree = assign_multidegree


def assign_divisions(G, E):  # G a PicGraph, E a list of edges to divide
    r"""
    Takes PicGraph G and list d of edges and returns the associated
    PicPhiGraph obtained by subdividing these edges.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import PicGraph, assign_divisions
        sage: G = PicGraph([1,3], [[1, 3], [2, 4]], [(1,2), (3,4)], [8, -9])
        sage: assign_divisions(G, [(1,2)])
        [(1, 8), (3, -9)] [[1, 3], [2, 4]] [(1, 2), (3, 4)] [(1, 2)]
    """
    return PicPhiGraph(G.genera(), G.legs(), G.edges(), [G.multideg, E])


def quasi_stable_multidegree_list(G, v, td=0):
    r"""
    Return the list of all stable multidegrees on a quasi-stable
    model of the stable graph G.

    Here v specifies the stability: it's either a vertex (a la Abreu-Pacini)
    or a stability condition (a la Kass-Pagani).

    EXAMPLES::

        sage: from admcycles import StableGraph
        sage: from admcycles.logtaut.picgraph import GraphStabilitySpace, quasi_stable_multidegree_list
        sage: V = GraphStabilitySpace(1, 2)
        sage: phi = V(0, {(1,2): 1/3, (1,): 2, (2,): -2})
        sage: G = StableGraph([0,0], [[1, 3, 5], [2, 4, 6]], [(3,4), (5,6)])
        sage: quasi_stable_multidegree_list(G, phi)
        [[(0, 2), (0, -2)] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] []]
    """
    # Not trying to do this at all efficiently for now; just want something that works.
    # valence_list = [len(stuff) for stuff in G.legs()]
    valence_list = [G.subset_valence([asd]) for asd in range(G.num_verts())]
    if isinstance(v, Integer):
        weight_list = [list(range(1 - x, x)) for x in valence_list]
    else:
        weight_list = [list(range(floor(v.weight(G, i) - x / ZZ(2)), ceil(v.weight(G, i) + x / ZZ(2))))
                       for i, x in enumerate(valence_list)]  # [[a for a in range(1-x,x)] for x in valence_list]
    edge_subsets = powerset(G.edges())
    output = []
    for WW in itertools.product(*weight_list):
        for EE in edge_subsets:
            WW = list(WW)
            if sum(WW) - len(EE) == td:
                Gphi = PicPhiGraph(G.genera(), G.legs(), G.edges(), [WW, EE])
                if Gphi.is_quasistable(v):
                    output.append(Gphi)
    return output


class FlowOnGraph(StableGraph):
    r"""
    FlowOnGraph is a (pre)stable graph together with a flow, i.e. an
    assignment w of integers (or symbolic expressions, ...) to its
    half-edges, satisfying w(h) + w(i(h)) = 0.
    """
    def __init__(self, genera, legs, edges, flow, mutable=False, check=True):
        r"""
        flow puts an integer (or symbolic, or ...) weight on each half-edge, satisfying
        w(h) + w(i(h)) = 0
         """
        StableGraph.__init__(self, genera, legs, edges,
                             mutable=mutable, check=check)
        self.flow = flow
        if check:
            for e in self.edges():
                if not self.flow[e[0]] + self.flow[e[1]] == 0:
                    raise ValueError(
                        "this is not a flow! Opposite half-edges should have opposite weight. ")

    def __repr__(self):
        r"""
        Returns string representation of FlowOnGraph.

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: WG = FlowOnGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], {1 : 2, 4: -2, 2:1, 5:-1, 3:0, 6:0, 7:4 })
            sage: WG
            [0, 0] [[1, 2, 3], [4, 5, 6, 7]] [(1, 4), (2, 5), (3, 6)] {1: 2, 4: -2, 2: 1, 5: -1, 3: 0, 6: 0, 7: 4}
        """
        return repr(self._genera) + ' ' + repr(self._legs) + ' ' + repr(self._edges) + ' ' + repr(self.flow)

    def copy(self, mutable=True):
        r"""
        Return a copy of this graph.

        When it is asked for an immutable copy of an immutable graph the
        current graph is returned without copy.

        INPUT:

        - ``mutable`` - (boolean, default ``True``) whether the returned graph must
          be mutable

        EXAMPLES::

            sage: from admcycles.logtaut import FlowOnGraph
            sage: WG = FlowOnGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], {1 : 2, 4: -2, 2:1, 5:-1, 3:0, 6:0, 7:0 })
            sage: WG2 = WG.copy()
            sage: WG2 == WG
            True
            sage: WG2.is_mutable()
            True
            sage: WG2 is WG
            False

            sage: WG.copy(mutable=False) is WG
            True
        """
        if not self.is_mutable() and not mutable:
            # avoid copy when immutability holds for both
            return self
        G = StableGraph.copy(self, mutable)
        return FlowOnGraph(G._genera, G._legs, G._edges, copy(self.flow), mutable)

    # def edge_list_format(self):
        # return [self.flow[e[0]] for e in self.edges()]

    def has_vanishing_outgoing_slopes(self):
        r"""
        Checks if slope vanishes at markings.

        EXAMPLES::

            sage: from admcycles.logtaut import FlowOnGraph
            sage: WG = FlowOnGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], {1 : 2, 4: -2, 2:1, 5:-1, 3:0, 6:0, 7:4 })
            sage: WG.has_vanishing_outgoing_slopes()
            False
            sage: WG = FlowOnGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], {1 : 2, 4: -2, 2:1, 5:-1, 3:0, 6:0, 7:0 })
            sage: WG.has_vanishing_outgoing_slopes()
            True
        """
        return all(self.flow[h] == 0 for h in self.list_markings())

    def zero_flow_contraction(self):
        r"""
        Return the FlowOnGraph obtained by contracting all edges of flow zero.

        EXAMPLES::

            sage: from admcycles.logtaut import FlowOnGraph
            sage: WG = FlowOnGraph([0,0],[[1,2,3],[4,5,6,7]],[(1,4),(2,5),(3,6)], {1 : 2, 4: -2, 2:1, 5:-1, 3:0, 6:0, 7:4 })
            sage: WG.zero_flow_contraction()
            [0] [[1, 2, 4, 5, 7]] [(1, 4), (2, 5)] {1: 2, 4: -2, 2: 1, 5: -1, 7: 4}
        """
        G = self.copy(mutable=True)
        for e in self.edges():
            if self.flow[e[0]] == 0:
                G.contract_edge(e)
                G.flow.pop(e[0])
                G.flow.pop(e[1])
        return G

    def _digraph(self, contract=False, dictionaries=False):
        r"""
        Return a Sage DiGraph representing the FlowOnGraph. It has a directed edge
        for each edge with positive flow and two directed edges in opposite direction
        for an edge with vanishing flow.

        INPUT:

        - ``contract`` -- (bool, default: False) return graph where edges of flow zero are contracted

        - ``dictionaries`` -- (bool, default: False) return tuple (Gamma, dicv, dice) mapping vertices and
          edges of the StableGraph to the vertices and edges of the DiGraph

        EXAMPLES::

            sage: from admcycles.logtaut import FlowOnGraph
            sage: WG = FlowOnGraph([0,1,2],[[1,2,3,4],[5,6,7,8],[9]],[(1,4),(2,5),(3,6),(8,9)], {1 : 2, 4: -2, 2:1, 5:-1, 3:0, 6:0, 7:0, 8:-3, 9:3 })
            sage: D = WG._digraph()
            sage: D.edges(sort=True)
            [(0, 0, 2), (0, 1, 0), (1, 0, 0), (1, 0, 1), (1, 2, 3)]
            sage: D2 = WG._digraph(contract=True)
            sage: D2.edges(sort=True)
            [(0, 0, 1), (0, 0, 2), (0, 1, 3)]
        """
        if contract:
            return self.zero_flow_contraction()._digraph(dictionaries=dictionaries)
        vertices = list(range(self.num_verts()))
        edges = []
        for e0, e1 in self.edges():
            # negative flow = going out of vertex, in direction of half-edge e0
            if self.flow[e0] <= 0:
                edges.append(
                    (self.vertex(e0), self.vertex(e1), -self.flow[e0]))
            if self.flow[e0] >= 0:  # positive flow = going into vertex, in direction of half-edge e0
                edges.append((self.vertex(e1), self.vertex(e0), self.flow[e0]))
            # for self.flow[e0]==0, both edges are added
        return DiGraph([vertices, edges], format='vertices_and_edges', multiedges=True, loops=True)

    def is_Suzumura_consistent(self):
        r"""
        Checks that the flow has no strict directed cycles.

        EXAMPLES::

            sage: from admcycles.logtaut import FlowOnGraph
            sage: WG = FlowOnGraph([1,1],[[1,2],[3,4]],[(1,3),(2,4)], {1 : 2, 2: 3, 3: -2, 4: -3})
            sage: WG.is_Suzumura_consistent()
            True
            sage: WG = FlowOnGraph([1,1],[[1,2],[3,4]],[(1,3),(2,4)], {1 : 2, 2: -3, 3: -2, 4: 3})
            sage: WG.is_Suzumura_consistent()
            False
            sage: WG = FlowOnGraph([1,1],[[1,2],[3,4]],[(1,3),(2,4)], {1 : 0, 2: -3, 3: 0, 4: 3})
            sage: WG.is_Suzumura_consistent()
            False
            sage: WG = FlowOnGraph([1,1],[[1,2],[3,4]],[(1,3),(2,4)], {1 : 0, 2: 0, 3: 0, 4: 0})
            sage: WG.is_Suzumura_consistent()
            True
            sage: WG = FlowOnGraph([1,1],[[1,2],[3,4,5,6]],[(1,3),(2,4),(5,6)], {1 : 0, 2: 0, 3: 0, 4: 0, 5 : 1, 6 : -1})
            sage: WG.is_Suzumura_consistent()
            False
            sage: WG = FlowOnGraph([1,1],[[1,2],[3,4,5,6]],[(1,3),(2,4),(5,6)], {1 : 0, 2: 0, 3: 0, 4: 0, 5 : 0, 6 : 0})
            sage: WG.is_Suzumura_consistent()
            True
            sage: WG = FlowOnGraph([1,1,1],[[1,2],[3,4],[5,6]],[(2,3),(4,5),(6,1)], {1:-1,6:1,2:-7,3:7,4:-1,5:1})
            sage: WG.is_Suzumura_consistent()
            True
            sage: WG = FlowOnGraph([1,1,1],[[1,2],[3,4],[5,6]],[(2,3),(4,5),(6,1)], {1:-1,6:1,2:7,3:-7,4:1,5:-1})
            sage: WG.is_Suzumura_consistent()
            False
        """
        return self._digraph(contract=True).is_directed_acyclic()

    def cone(self):
        r"""
        Returns cone of edge-length-assignments such that the flow can be realised by
        a PL function on the graph with those edge-lengths.

        Cf. Lemma 4.4 of [Holmes, Extending the double ramiﬁcation cycle by resolving the Abel-Jacobi map, JIMJ].

        EXAMPLES::

            sage: from admcycles.logtaut import *
            sage: FG = FlowOnGraph([0,0],[[1,2],[4,5]],[(1,4),(2,5)], {1 : 2, 4: -2, 2:1, 5:-1})
            sage: FG.cone().rays()
            N(1, 2)
            in 2-d lattice N
            sage: FG = FlowOnGraph([0,0,0],[[1,2],[4,5,6],[7]],[(1,4),(6,7)], {1 : 2, 4: -2, 2:1, 5:-1, 6:0, 7:0})
            sage: FG.cone().rays()
            N(0, 1),
            N(1, 0)
            in 2-d lattice N
        """
        e = self.num_edges()
        NN = ToricLattice(e)
        B = self.cycle_basis()
        d = len(self.edges())
        vlist = []
        for j in range(d):
            a = [0 for _ in range(d - 1)]
            a.insert(j, 1)
            vlist.append(a)
        for b in B:
            vlist.append([b[i] * self.flow[self.edges()[i][0]]
                         for i in range(len(b))])
            vlist.append([-b[i] * self.flow[self.edges()[i][0]]
                         for i in range(len(b))])
        return Cone(vlist, NN.dual()).dual()

    def integrate(self, v, edge_length_dict, val_at_v=0):
        r"""
        Integrate the flow starting at vertex v, with respect to the
        given dictionary of edge lengths. Return the list of values
        of the function at the vertices.

        This assumes that the given edge lengths lead to a consistent
        function (i.e. no path-dependence exists).

        EXAMPLES::

            sage: from admcycles.logtaut import FlowOnGraph
            sage: G = FlowOnGraph([0,0,0], [[1,3],[2,4,5],[6]], [(1,2),(3,4),(5,6)], {1:3,2:-3,3:2,4:-2,5:-7,6:7})
            sage: G.integrate(0,{(1,2):4,(3,4):6,(5,6):7})
            [0, 12, -37]
            sage: G.integrate(0,{(1,2):4,(3,4):6,(5,6):2})
            [0, 12, -2]
        """
        resultdict = {v: val_at_v}
        nvert = len(self.genera())
        done = False
        HH = self.legs()
        while not done:
            donevertices = list(resultdict)
            for e in self.edges():
                for vv in donevertices:
                    if e[0] in HH[vv]:
                        outv = [a for a in range(nvert) if e[1] in HH[a]][0]
                        resultdict[outv] = resultdict[vv] + \
                            self.flow[e[0]] * edge_length_dict[e]
                    if e[1] in HH[vv]:
                        outv = [a for a in range(nvert) if e[0] in HH[a]][0]
                        resultdict[outv] = resultdict[vv] + \
                            self.flow[e[1]] * edge_length_dict[e]
            if len(resultdict) == len(self.genera()):
                done = True
        return [resultdict[i] for i in range(nvert)]


def flow_from_list(G, ll):
    r"""
    Take a StableGraph and a list of integers of length equal to number of edges,
    and outputs a FlowOnGraph,  with zero flow on outgoing legs.

    INPUT:

    - ``G`` -- (StableGraph)
    - ``ll`` -- (list) must have same length as number of edges of G

    EXAMPLES::

        sage: from admcycles import StableGraph
        sage: from admcycles.logtaut.picgraph import flow_from_list
        sage: G = StableGraph([0,0], [[1, 3, 5], [2, 4, 6]], [(3,4), (5,6)])
        sage: flow_from_list(G, [2,4,7])
        [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] {3: 2, 4: -2, 5: 4, 6: -4, 1: 0, 2: 0}
    """
    flowdict = {}
    for e in range(len(G.edges())):
        flowdict[G.edges()[e][0]] = ll[e]
        flowdict[G.edges()[e][1]] = -ll[e]
    for h in G.list_markings():
        flowdict[h] = 0
    return FlowOnGraph(G.genera(), G.legs(), G.edges(), flowdict)


def KassPagani_basis(g, n):
    r"""
    Return a list of basis vectors.

    Parameters
    ----------

    g : int
        genus
    n : int
        number of markinga

    Returns
    -------

    A list which gives the basis vectors, of the forms:

    - (h,m) : tuple of genus h and tuple m of markings
      convention: 1 in m (for n>0), or h<g/2 (for n=0)
      [gives weight of associated vertex in graph with 2 vertices, 1 edge]
    - (i,)  : tuple of integer i, giving index of a marking
      connected by 2 edges, with genus 0 vertex carrying precisely marking i
      [gives weight of associated vertex in graph with 2 vertices, 1 edge]

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import KassPagani_basis
        sage: KassPagani_basis(0, 4)
        [(0, (1, 2)), (0, (1, 3)), (0, (1, 4))]
        sage: KassPagani_basis(1, 2)
        [(0, (1, 2)), (1,)]
        sage: KassPagani_basis(2, 0)
        []
        sage: KassPagani_basis(3, 0)
        [(1, ())]
        sage: KassPagani_basis(4, 0)
        [(1, ())]
        sage: KassPagani_basis(5, 0)
        [(1, ()), (2, ())]
    """
    assert 2 * g - 2 + n > 0
    if n == 0:
        output = [(h, ()) for h in range(1, (g - 1) // 2 + 1)]
    else:
        output = []
        for h in range(g + 1):
            for m in powerset(range(2, n + 1)):
                m.append(1)
                m.sort()
                if 2 * h - 2 + len(m) + 1 > 0 and 2 * (g - h) - 2 + n - len(m) + 1 > 0:
                    output.append((h, tuple(m)))
    if g == 0:
        return output
    if g == 1:
        nmax = n - 1
    else:
        nmax = n
    output.extend((i,) for i in range(1, nmax + 1))
    return output


class GraphStabilityCondition(ModuleElement):
    r"""
    A Kass-Pagani stability condition.
    """
    def __init__(self, parent, d, degdict):
        r"""
        INPUT:

        parent : a class:`GraphStabilitySpace`
        d : total degree
        degdict : a dictionary

        The dictionary degdict assigns vertex weights for basic graphs (corresponding
        to a basis of the space of stability conditions). Its keys are::

            - (h,m) : tuple of genus h and tuple m of markings
                      gives weight of associated vertex in graph with 2 vertices, 1 edge
                      convention: 1 in m (for n>0), or h<g/2 (for n=0)
            - (i,)  : tuple of integer i, giving index of a marking
                      gives weight of vertex containing i in graph with 2 vertices (genera 0, g-1)
                      connected by 2 edges, with genus 0 vertex carrying precisely marking i

        EXAMPLES::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: from admcycles import StableGraph
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: G = StableGraph([1,0],[[5],[3,1,2]],[(5,3)])
            sage: phi.weight(G,1)
            3
            sage: phi.weight(G,0)
            -3

        TESTS::

            sage: from admcycles.logtaut import *
            sage: from admcycles import list_strata
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: for e in range(3):
            ....:     for gamma in list_strata(1,2,e):
            ....:         for v in range(gamma.num_verts()):
            ....:             print(gamma,v,phi.weight(gamma,v))
            ....:
            [1] [[1, 2]] [] 0 0
            [0] [[1, 2, 3, 4]] [(3, 4)] 0 0
            [0, 1] [[1, 2, 3], [4]] [(3, 4)] 0 3
            [0, 1] [[1, 2, 3], [4]] [(3, 4)] 1 -3
            [0, 0] [[1, 2, 5], [3, 4, 6]] [(3, 4), (5, 6)] 0 3
            [0, 0] [[1, 2, 5], [3, 4, 6]] [(3, 4), (5, 6)] 1 -3
            [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] 0 5
            [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] 1 -5
        """
        ModuleElement.__init__(self, parent)

        degdict = copy(degdict)

        # g=1 special treatment
        if self.parent()._g == 1:
            n = self.parent()._n
            missing = [(i,) for i in range(1, n + 1) if (i,) not in degdict]
            if len(missing) > 1:
                raise ValueError(
                    'must specify degree on vertex (i,) for all but at most one marking i')
            if len(missing) == 1:
                degdict[missing[0]] = d - sum(degdict[(i,)]
                                              for i in range(1, n + 1) if (i,) != missing[0])

        self._degdict = degdict
        self.d = d

        @cached_function
        def zero_weight(gamma, v):
            r"""
            Underlying weight function before degree shift.
            """
            g = gamma.g()
            n = gamma.n()
            # If gamma is not in canonical_label form, call the function again
            H = gamma.copy()
            dicv, _ = H.set_canonical_label(certificate=True)
            H.set_immutable()
            if gamma != H:
                return zero_weight(H, dicv[v])

            # TODO: check that g,n agree with parent g,n
            if gamma.num_verts() == 1:
                return ZZ.zero()

            if gamma.num_verts() == 2 and gamma.num_edges() == 1:
                if n == 0:
                    if gamma.genera(v) < gamma.genera(1 - v):
                        return degdict[(gamma.genera(v), ())]
                    elif gamma.genera(v) == gamma.genera(1 - v):
                        return ZZ.zero()
                    else:
                        return -degdict[(gamma.genera(1 - v), ())]
                if 1 not in gamma.legs(v):
                    v = 1 - v
                    sign = -ZZ.one()
                else:
                    sign = ZZ.one()
                return sign * degdict[(gamma.genera(v), gamma.list_markings(v))]

            if sorted(gamma.genera()) == [0, g - 1] and gamma.num_edges() == 2:
                u = gamma.genera().index(0)
                if len(gamma.legs(u)) == 3 and len(gamma.list_markings(u)) == 1:
                    j = gamma.list_markings(u)
                    sign = ZZ(-1)**(u != v)
                    return sign * degdict[j]

            # The above concludes the input of the base cases from degdict
            # Now we need to start the actual recursion
            # First we contract all edges not having EXACTLY one half-edge at v and see if this changes the graph
            # if so, we compute the weight of v as - sum of other weights
            H = gamma.copy()
            dicv = H.contract_edge([e for e in gamma.edges() if sum(
                1 for a in e if a in gamma.legs(v)) != 1], adddata=True)

            if H != gamma or gamma.num_verts() > 2:
                H.set_immutable()
                return -sum([zero_weight(H, w) for w in range(H.num_verts()) if w != dicv[v]], ZZ.zero())

            # Now we know that gamma has precisely two vertices, at least two edges and no self-edges
            if n == 0:
                return ZZ.zero()  # see Kass-Pagani, Proposition 3.9
            # Compute E3 = weight of genus zero vertex in (g=0) -3 edges- (g-2; 1,...,n)
            if g >= 2:
                E3 = -sum(degdict[(i,)]
                          for i in range(1, n + 1)) / ZZ(2 * g - 2)
            else:
                E3 = 0  # will not appear with nonzero coefficient
            e = gamma.num_edges()
            h = gamma.genera(v)
            return (e - 2 + 2 * h) * E3 + sum(degdict[(i,)] for i in gamma.list_markings(v))

        self._zero_weight = zero_weight

    def __repr__(self):
        return "A Kass-Pagani stability condition in genus " + str(self.parent()._g) + " with " + str(self.parent()._n) + " markings. \n The degree dictionary is " + str(self._degdict) + "."

    def weight(self, gamma, v):
        r"""
        Gives the weight of vertex v inside the stable graph gamma.

        EXAMPLES::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: from admcycles import StableGraph
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: G = StableGraph([0,0], [[1,2,3], [4,5,6]], [(3,4), (5,6)])
            sage: phi.weight(G, 0)
            3
            sage: phi5 = V(5,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: phi5.weight(G, 0)
            8
            sage: phi5.weight(G, 1)
            -3
        """
        g = gamma.g()
        zw = self._zero_weight(gamma, v)
        if g != 1:
            return zw + self.d / (ZZ(2) * g - 2) * (ZZ(2) * gamma.genera(v) - 2 + gamma.num_legs(v))
        return zw + self.d * ZZ(1 in gamma.legs(v))

    def _compatibility_check(self, maxedge=None):
        r"""
        Checks compatibility of the stability condition with all edge contractions
        of stable graphs (for up to maxedge edges).

        EXAMPLES::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(3,2)
            sage: phi = V(0,{(0,(1,2)):17, (1,(1,)):9, (1,(1,2)):-4, (2,(1,)):-2, (2,(1,2)):0, (1,):0, (2,):-9})
            sage: phi._compatibility_check()
            True
            sage: phi = V(5,{(0,(1,2)):17, (1,(1,)):9, (1,(1,2)):-4, (2,(1,)):-2, (2,(1,2)):0, (1,):0, (2,):-9})
            sage: phi._compatibility_check()
            True
            sage: V = GraphStabilitySpace(2,2)
            sage: phi = V(9,{(0,(1,2)):17, (1,(1,)):9, (1,(1,2)):-4, (1,):0, (2,):-9})
            sage: phi._compatibility_check()
            True
            sage: V = GraphStabilitySpace(3,0)
            sage: phi = V(0,{(1,()):8})
            sage: phi._compatibility_check()
            True
            sage: V = GraphStabilitySpace(5,0)
            sage: phi = V(-1,{(1,()):8, (2,()):-3})
            sage: phi._compatibility_check(5)
            True
            sage: V = GraphStabilitySpace(0,5)
            sage: phi = V(17,{(0,(1,2)):8, (0,(1,3)):3, (0,(1,4)):-1, (0,(1,5)):8, (0,(1,2,3)):9, (0,(1,2,4)):-3, (0,(1,2,5)):0, (0,(1,3,4)):-5, (0,(1,3,5)):2, (0,(1,4,5)):-133})
            sage: phi._compatibility_check()
            True
        """
        g, n = self.parent()._g, self.parent()._n
        if maxedge is None:
            maxedge = 3 * g - 3
        for d in range(1, maxedge + 1):
            for gamma in list_strata(g, n, d):
                for e in gamma.edges():
                    H = gamma.copy()
                    av, _, vnum, _ = H.contract_edge(e, adddata=True)
                    H.set_immutable()
                    if not sum(self.weight(gamma, w) for w in vnum) == self.weight(H, av):
                        return (False, gamma, e)
        return True

    def copy(self):
        P = self.parent()
        return P.element_class(P, {g: t.copy() for g, t in self._terms.items()})

    def __neg__(self):
        r"""
        Negative of class.

        EXAMPLES::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: phi
            A Kass-Pagani stability condition in genus 1 with 2 markings.
             The degree dictionary is {(0, (1, 2)): 3, (1,): 5, (2,): -5}.
            sage: -phi
            A Kass-Pagani stability condition in genus 1 with 2 markings.
             The degree dictionary is {(0, (1, 2)): -3, (1,): -5, (2,): 5}.
        """
        P = self.parent()
        return P.element_class(P, -self.d, {k: -self._degdict[k] for k in self._degdict})

    def _add_(self, other):
        r"""
        Addition of stability conditions.

        TESTS::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(1,2)
            sage: phi1 = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: phi2 = V(0,{(0,(1,2)):-1, (1,):5, (2,):-2})
            sage: phi1 + phi2
            A Kass-Pagani stability condition in genus 1 with 2 markings.
             The degree dictionary is {(0, (1, 2)): 2, (1,): 10, (2,): -7}.
        """
        P = self.parent()
        return P.element_class(P, self.d + other.d, {k: self._degdict[k] + other._degdict[k] for k in self._degdict})

    def _lmul_(self, other):
        r"""
        Scalar multiplication by ``other``.

        TESTS::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(1,2)
            sage: phi1 = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: 3*phi1
            A Kass-Pagani stability condition in genus 1 with 2 markings.
             The degree dictionary is {(0, (1, 2)): 9, (1,): 15, (2,): -15}.
        """
        P = self.parent()
        assert parent(other) is self.base_ring()
        return P.element_class(P, other * self.d, {k: other * self._degdict[k] for k in self._degdict})

    def __bool__(self):
        r"""
        Check if stability condition is nonzero.

        TESTS::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(3,2)
            sage: phi = V(0,{(0,(1,2)):17, (1,(1,)):9, (1,(1,2)):-4, (2,(1,)):-2, (2,(1,2)):0, (1,):0, (2,):-9})
            sage: bool(phi)
            True
            sage: bool(0*phi)
            False
        """
        return not (self.d.is_zero() and not any(self._degdict[k] for k in self._degdict))

    # Python 2 support
    __nonzero__ = __bool__

    def __eq__(self, other):
        r"""
        Check equality of stability conditions.

        TESTS::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(3,2)
            sage: V = GraphStabilitySpace(1,2)
            sage: phi1 = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: L = V.basis(); L
            [A Kass-Pagani stability condition in genus 1 with 2 markings.
              The degree dictionary is {(0, (1, 2)): 1, (1,): 0, (2,): 0}.,
             A Kass-Pagani stability condition in genus 1 with 2 markings.
              The degree dictionary is {(0, (1, 2)): 0, (1,): 1, (2,): -1}.]
            sage: phi2 = 3*L[0] + 5*L[1]
            sage: phi1 == phi2
            True
        """
        return isinstance(other, GraphStabilityCondition) and \
            self.parent() == other.parent() and \
            not (self - other)

    def __ne__(self, other):
        r"""
        Check non-equality of stability conditions.

        TESTS::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(3,2)
            sage: V = GraphStabilitySpace(1,2)
            sage: phi1 = V(0,{(0,(1,2)):3, (1,):5, (2,):-5})
            sage: L = V.basis(); L
            [A Kass-Pagani stability condition in genus 1 with 2 markings.
              The degree dictionary is {(0, (1, 2)): 1, (1,): 0, (2,): 0}.,
             A Kass-Pagani stability condition in genus 1 with 2 markings.
              The degree dictionary is {(0, (1, 2)): 0, (1,): 1, (2,): -1}.]
            sage: phi2 = 3*L[0] + 4*L[1]
            sage: phi1 != phi2
            True
        """
        return not (self == other)


class GraphStabilitySpace(UniqueRepresentation, Parent):
    r"""
    The space of Kass-Pagani stability conditions.
    """
    Element = GraphStabilityCondition

    @staticmethod
    def __classcall__(cls, g, n=None, base_ring=None):
        if not isinstance(g, numbers.Integral) or g < 0:
            raise ValueError('g must be a non-negative integer')
        g = ZZ(g)
        if n is None:
            n = ZZ.zero()
        elif isinstance(n, numbers.Integral):
            if n < 0:
                raise ValueError('n must be a non-negative integer')
            n = ZZ(n)
        else:
            raise TypeError('invalid input')

        if base_ring is None:
            base_ring = QQ
        elif base_ring not in _CommutativeRings:
            raise ValueError(
                'base_ring (={}) must be a commutative ring'.format(base_ring))

        if (g, n) in [(0, 0), (0, 1), (0, 2), (1, 0)]:
            raise ValueError('unstable pair (g,n) = ({}, {})'.format(g, n))

        return super(GraphStabilitySpace, cls).__classcall__(cls, g, n, base_ring)

    def __init__(self, g, n, base_ring):
        r"""
        INPUT:

        g : integer
          genus
        n : integer
          number of markings
        base_ring : ring
          base ring
        """
        Parent.__init__(self, base=base_ring, category=VectorSpaces(base_ring))
        self._g = g
        self._n = n

    def _coerce_map_from_(self, other):
        r"""
        TESTS::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: GraphStabilitySpace(1, 1, base_ring=CC).has_coerce_map_from(GraphStabilitySpace(1, 1, base_ring=QQ))
            True
        """
        if isinstance(other, GraphStabilitySpace) and \
           self._g == other._g and \
           self._n == other._n and \
           self.base_ring().has_coerce_map_from(other.base_ring()):
            return True

    def _base_tuples(self):
        return KassPagani_basis(self._g, self._n)

    def basis(self, d=0):
        r"""
        Returns a basis of stability conditions in degree d.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import GraphStabilitySpace
            sage: V = GraphStabilitySpace(1, 2)
            sage: V.basis()
            [A Kass-Pagani stability condition in genus 1 with 2 markings.
              The degree dictionary is {(0, (1, 2)): 1, (1,): 0, (2,): 0}.,
             A Kass-Pagani stability condition in genus 1 with 2 markings.
              The degree dictionary is {(0, (1, 2)): 0, (1,): 1, (2,): -1}.]
        """
        bt = self._base_tuples()
        return [self(d, {c: ZZ(b == c) for c in bt}) for b in bt]

    def _an_element_(self):
        return sum(self.basis(), self.zero())

    def some_elements(self):
        r"""
        Return some elements in this GraphStabilitySpace.

        This is mostly used for sage test system.

        EXAMPLES::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: _ = GraphStabilitySpace(0, 5).some_elements()
            sage: _ = GraphStabilitySpace(2, 3).some_elements()
        """
        return self.basis() + [sum(self.basis(), self.zero())]

    def zero(self):
        r"""
        Return the zero element.

        EXAMPLES::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: GraphStabilitySpace(3,1).zero()
            A Kass-Pagani stability condition in genus 3 with 1 markings.
             The degree dictionary is {(1, (1,)): 0, (2, (1,)): 0, (1,): 0}.
        """
        return self.element_class(self, self.base_ring().zero(), {k: self.base_ring().zero() for k in self._base_tuples()})

    def _element_constructor_(self, d, degdict):
        r"""
        TESTS::

            sage: from admcycles.logtaut import GraphStabilitySpace
            sage: V = GraphStabilitySpace(3,1)
            sage: V(0, {(1, (1,)): 2, (2, (1,)): 5, (1,): 6})
            A Kass-Pagani stability condition in genus 3 with 1 markings.
             The degree dictionary is {(1, (1,)): 2, (2, (1,)): 5, (1,): 6}.
        """
        return self.element_class(self, d, degdict)


# def naive_completeness_check(FF, n):
    # d = FF.dim()
    # NN = range(n)
    # out = []
    # for v in itertools.product(NN, repeat=d):
    #     if not FF.support_contains(v):
    #         out.append(v)
    #         print("missing", v)
    # return out

# def is_affine_complete_old(FF):
    # d = FF.lattice_dim()
    # posvlist = []
    # for j in range(d):
    # a = [0 for i in range(d-1)]
    # a.insert(j, 1)
    # posvlist.append(a)
    # negvlist = [[-b for b in a] for a in posvlist]
    # conelist = [c for C in FF.cones() for c in C]
    # for S in powerset(range(d)):
    # if len(S) != 0:
    # rays = [negvlist[i] for i in S] + [posvlist[i] for i in range(d) if not i in S]
    # conelist.append(Cone(rays, lattice = FF.lattice()))
    # try:
    # return Fan(conelist, discard_faces = True).is_complete()
    # except ValueError:  # can happen if FF union the cones above
    # return False


def cone_contains(P, Q):
    r"""
    Checks if cone P contains cone Q.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import cone_contains
        sage: C = Cone([(1,0),(0,1)])
        sage: D = Cone([(1,0),(1,1)])
        sage: cone_contains(C,D)
        True
        sage: cone_contains(D,C)
        False
    """
    return all(P.contains(v) for v in Q.rays())


def is_affine_complete(FF):
    r"""
    Checks whether a fan in the positive orthant completely fills the interior of that orthant.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import is_affine_complete
        sage: is_affine_complete(Fan([Cone([[1,0],[0,1]])]))
        True
        sage: C1 = Cone([[1,0],[1,1]])
        sage: C2 = Cone([[0,1],[1,1]])
        sage: F = Fan([C1, C2], discard_faces=True)
        sage: is_affine_complete(F)
        True
        sage: F = Fan([C1])
        sage: is_affine_complete(F)
        False
        sage: F = Fan([Cone([[1,0,0],[0,1,0],[0,0,1]])])
        sage: F2 = F.subdivide(new_rays=[(1,1,0)])
        sage: is_affine_complete(F2)
        True
    """
    d = FF.lattice_dim()
    Orth = positive_orthant(d)
    Facets = Orth.facets()
    if FF.dim() != d:
        return False
    for cone in FF:
        if cone.dim() != d or not cone_contains(Orth, cone):
            return False
    # Now we know that all generating cones are full-dimensional and contained in the positive Orthant.
    # Then boundary cones are (d-1)-dimensional.
    for cone in FF(codim=1):
        if len(cone.star_generator_indices()) != 2 and not any(cone_contains(Fa, cone) for Fa in Facets):
            # cone is not contained in any facet of the orthant, and also not contained in precisely two d-dimensional cones
            return False
    # If any point in the (strict) positive orthant was not contained in FF, we could draw a line from that to a generic point
    # in FF and would hit a facet of FF not contained in any of the facets of the orthant and only bounding on one maximal cone
    # of FF; since this is excluded, the fan is affine complete.
    return True

# from Aaron Pixton:


def acyclic_flows(weights, legs, edges):
    r"""
    Calculates the acyclic flows on a graph, balancing out a given
    set of weights on the vertices (which must sum to zero).

    INPUT:

    - weights: (list) list of integer weights on the vertices
    - legs: (list) list of legs of the vertices
    - edges: (list) list of pairs of legs connected by an edge

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import acyclic_flows
        sage: weights = [5, -3, -2]
        sage: legs = [[1,3], [2,4,5], [6]]
        sage: edges = [(1,2), (3,4), (5,6)]
        sage: acyclic_flows(weights, legs, edges)
        [{1: 4, 2: -4, 3: 1, 4: -1, 5: 2, 6: -2},
         {1: 3, 2: -3, 3: 2, 4: -2, 5: 2, 6: -2},
         {1: 2, 2: -2, 3: 3, 4: -3, 5: 2, 6: -2},
         {1: 1, 2: -1, 3: 4, 4: -4, 5: 2, 6: -2}]
    """
    def leg_in_edge(l):
        for e in edges:
            if l == e[0] or l == e[1]:
                return True
        return False
    Linternal = [[l for l in L if leg_in_edge(l)] for L in legs]
    Loutgoing = {l for L in legs for l in L if not leg_in_edge(l)}
    internal_dicts = acyclic_recur(
        weights, [0 for _ in range(len(weights))], Linternal, edges, {})
    for DD in internal_dicts:
        for L in Loutgoing:
            DD[L] = 0
    return internal_dicts

# notes on source_constraints logic:
# start out all at 0
# -3: not in current set
# -2: not in current set but will be a candidate for next set
# -1: could be dragged by a 0 edge into the current set
#  0: candidate for current set
#  1: definitely in current set


def acyclic_recur(weights, source_constraints, legs, edges, flow_dict):
    # print([weights,source_constraints,flow_dict])
    ans_list = []
    n = len(weights)
    maxc = max(source_constraints)
    if maxc < 0:
        for i in range(n):
            if source_constraints[i] <= -2:
                source_constraints[i] += 2
    maxc = max(source_constraints)
    if maxc < 0:
        if max(weights) == 0 and len(flow_dict) == 2 * len(edges):
            return [flow_dict]
        return []
    i = next(j for j in range(n) if source_constraints[j] >= 0)
    if source_constraints[i] == 0 and any(x not in flow_dict for x in legs[i]):
        new_weights = copy(weights)
        new_constraints = copy(source_constraints)
        new_flow_dict = copy(flow_dict)
        new_constraints[i] = -3
        ans_list += acyclic_recur(new_weights,
                                  new_constraints, legs, edges, new_flow_dict)
    if weights[i] >= 0:
        Lincident = [x for x in legs[i] if x not in flow_dict]
        k = len(Lincident)
        Lpair = []
        for x in Lincident:
            for e in edges:
                if e[0] == x:
                    Lpair.append(e[1])
                    break
                if e[1] == x:
                    Lpair.append(e[0])
                    break
        Lvert = []
        for l in Lpair:
            for j in range(n):
                if l in legs[j]:
                    Lvert.append(j)
                    break
        for new_flow in IntegerVectors(weights[i], k):
            violates_constraints = False
            for j in range(k):
                if new_flow[j] == 0:
                    if source_constraints[Lvert[j]] < -1:
                        violates_constraints = True
                        break
                else:
                    if source_constraints[Lvert[j]] == 1:
                        violates_constraints = True
                        break
            if violates_constraints:
                continue
            zero_set = {Lvert[j] for j in range(k) if new_flow[j] == 0}
            if any(Lvert[j] in zero_set for j in range(k) if new_flow[j] > 0):
                continue
            new_weights = copy(weights)
            new_constraints = copy(source_constraints)
            new_flow_dict = copy(flow_dict)
            for j in range(k):
                x = new_flow[j]
                new_weights[Lvert[j]] += x
                new_flow_dict[Lincident[j]] = x
                new_flow_dict[Lpair[j]] = -x
                if x == 0:
                    new_constraints[Lvert[j]] = 1
                else:
                    new_constraints[Lvert[j]] = -2
            new_weights[i] = 0
            new_constraints[i] = -1
            ans_list += acyclic_recur(new_weights,
                                      new_constraints, legs, edges, new_flow_dict)
    return ans_list

# The purpose of the next function is to translate from a PL function on the cone of the subdivided graph to a PL function on the cone of the original graph, given the additional data of a flow.
# IT gives the matrix of a linear function from edges of small graph to edges of subdivided graph.
# It is uniqeuly determined by the following two properties:
# - when you compose with the `summation map' you get the identity on the edges of the original graph
# - it takes ever vector coming by (pairing a flow with a directed cycle) to zero.


def subdivided_edges_to_lin_combs_of_edges(G, EG, FF):
    r"""
    Translate from a PL function on the cone of the subdivided graph EG
    to a PL function on the cone of the original graph G, given the
    additional data of a flow FF.

    Returns the matrix of a linear function from edge lengths of small graph
    to edge lengths of subdivided graph.

    It is uniquely determined by the following two properties:

    - when you compose with the ``summation map`` you get the identity
      on the edges of the original graph
    - the TRANSPOSE of M takes every vector coming by (pairing a flow with a directed cycle) to zero.

    INPUT:

    - G:  (StableGraph) a stable graph
    - EG: (StableGraph) a subdivision of G;
      assume: new vertices appended at end of list of vertices for G,
      edges are built by inserting new legs inside the old edge
    - FF: (dict) a flow on the edges of EG

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import subdivided_edges_to_lin_combs_of_edges, assign_divisions
        sage: from admcycles import StableGraph
        sage: G = StableGraph([0,0], [[1,3,5],[2,4,6]],[(3,4),(5,6)])
        sage: EG = StableGraph([0,0,0], [[1,3,5],[2,4,6],[7,8]],[(3,7),(5,6),(8,4)])
        sage: FF = {3:3,7:-3,5:2,6:-2,8:-2,4:2}
        sage: subdivided_edges_to_lin_combs_of_edges(G, EG, FF)
        [ 2/5  2/5]
        [   0    1]
        [ 3/5 -2/5]
    """
    # D to add doctest here!
    # Here G is a graph, EG is a subdivision of E, and F is a flow on EG.
    # I'm assuming that EG has the new vertices appended at the end of the vertex list,
    # and that the new edges are built by inserting new legs inside the old edge (as in the construction in the above code).
    nE = len(G.edges())
    nEtilde = len(EG.edges())
    # output will be an ne x netilde matrix with rational (maybe integer) entries.
    B = EG.cycle_basis()
    flow_span_basis = []
    for gamma in B:
        Dvect = [FF[EG.edges()[e][0]] * gamma[e]
                 for e in range(nEtilde)]
        # for i in range(len(gamma)):
        #     Dvect.append(gamma[i] * FF[EG.edges()[i][0]])
        flow_span_basis.append(Dvect)
    M2_rows = []
    for e in G.edges():
        Dvect = []
        for se in EG.edges():
            if e == se or e[0] == se[1] or e[1] == se[0] or e[0] == se[0] or e[1] == se[1]:
                Dvect.append(1)
            else:
                Dvect.append(0)
        M2_rows.append(Dvect)
    Mrows = flow_span_basis + M2_rows
    M = Matrix(Mrows)
    outmatrows = []
    for i in range(nE):
        Vi = vector(QQ, [0 for a in B] + [0 for a in range(i)
                                          ] + [1] + [0 for a in range(nE - i - 1)])
        # outmatrows.append(Vi)
        outmatrows.append(M.solve_right(Vi))
    return Matrix(outmatrows).transpose()


"""
now moving to PL functions
"""

# Jo: I think that after giving up on the composition of morphisms below, the data type FiniteCat is currently just a kind of wrapper around the
# directed graph underlying it; and that directed graph D in fact has all the information needed:
# D.vertices() gives the list of objects, and each edge is of the form (domain of morphism, target of morphism, morphism)


class FiniteCat(DiGraph):
    def __init__(self, objects, gen_morphisms, check=False):
        r"""
        A (non-full!) subcategory of some given category. We give
        a finite list of objects and of morphisms between them.

        INPUT:

        - ``objects`` -- (list) a list of objects; should all have same parent.
        - ``gen_morphisms`` -- (list) a list of objects; sources and targets should
          be among the objects. We will add in composites automatically. We require
          these to have a domain() and a codomain() which are among the given objects.
        """
        self.objects = objects
        self.gen_morphisms = gen_morphisms
        graph_dict = {A: defaultdict(list) for A in objects}
        for F in gen_morphisms:
            graph_dict[F.domain()][F.codomain()].append(F)
        DiGraph.__init__(
            self, data=graph_dict, format='dict_of_dicts', loops=True, vertex_labels=True, multiedges=True, weighted=True)
        if check:
            return NotImplementedError, "could try to check all hom sets are finite, but that sounds like a lot of work! Also if not finite then algorithm may not terminate (word problem for groups)"

    def __repr__(self):
        return "Finite category with " + str(len(self.objects)) + " objects. "

    def composite(self, p1, p2):
        r"""
        Compose of two paths. Here the ordering is important. This is
        supposed to be consistent with writing e1*e2 if e1 and e2 are
        functions. In other words, we assume that e2 goes from a to b
        and e1 goes from b to c, and the composite goes from a to c.
        """
        return NotImplementedError

    def hom_set(self, v1, v2):
        r"""
        Homomorphisms from first to second object.
        """
        return NotImplementedError("this is basically word problem for group presentations")

    # def objects(self):
        # # don't we want to return self.objects here ? Also not good to call function same thing as internal variable objects
        # return list(self.graph_dict)

    def generating_morphisms(self):
        return self.gen_morphisms


class ConeMorphism():
    r"""
    A morphism between rational polyhedral cones.
    """
    def __init__(self, domain_cone, codomain_cone, latticemap, check=True):
        self.domain_cone = domain_cone
        self.codomain_cone = codomain_cone
        self.latticemap = latticemap
        if check:
            assert latticemap in Hom(
                domain_cone.lattice(), codomain_cone.lattice())
            FanMorphism(latticemap, Fan([domain_cone]), codomain=Fan([codomain_cone]))

    def compose(self, CM, check=True):
        r"""
        Composing two cone morphisms.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import ConeMorphism
            sage: C3 = Cone([[0,1,0], [1,0,1]])
            sage: C2 = Cone([[0,1],[1,0]])
            sage: C1 = Cone([[1]])
            sage: CM32 = ConeMorphism(C3, C2, Hom(C3.lattice(), C2.lattice())([[1,0],[0,1],[0,0]]))
            sage: CM21 = ConeMorphism(C2, C1, Hom(C2.lattice(), C1.lattice())([[1],[0]]))
            sage: CM31 = ConeMorphism(C3, C1, Hom(C3.lattice(), C1.lattice())([[1],[0], [0]]))
            sage: CM21.compose(CM32, check = True) == CM31
            True
            """
        assert self.domain_cone == CM.codomain_cone
        return ConeMorphism(CM.domain_cone, self.codomain_cone, self.latticemap * CM.latticemap, check=check)

    def __eq__(self, other):
        if not self.domain_cone == other.domain_cone:
            return False
        if not self.codomain_cone == other.codomain_cone:
            return False
        for R in self.domain_cone.rays():
            if not self.latticemap(R) == other.latticemap(R):
                return False
        return True

    def domain(self):
        r"""
        Domain of cone morphisms.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import ConeMorphism
            sage: C1 = Cone([[0,1],[1,0]])
            sage: C2 = Cone([[1]])
            sage: CM12 = ConeMorphism(C1, C2, Hom(C1.lattice(), C2.lattice())([[1],[0]]))
            sage: CM12.domain()
            2-d cone in 2-d lattice N
            sage: sorted(CM12.domain().rays())
            [N(0, 1), N(1, 0)]
            """
        return self.domain_cone

    def codomain(self):
        r"""
        Codomain of cone morphisms.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import ConeMorphism
            sage: C1 = Cone([[0,1],[1,0]])
            sage: C2 = Cone([[1]])
            sage: CM12 = ConeMorphism(C1, C2, Hom(C1.lattice(), C2.lattice())([[1],[0]]))
            sage: CM12.codomain()
            1-d cone in 1-d lattice N
            sage: sorted(CM12.codomain().rays())
            [N(1)]
            """
        return self.codomain_cone


class AbstractFanMorphism():
    def __init__(self, latticemap, domain_fan, codomain_fan, check=True):
        self.fan_morphism = FanMorphism(
            latticemap, domain_fan, codomain_fan, check=check)
        self.latticemap = latticemap
        self.domain_fan = domain_fan
        self.codomain_fan = codomain_fan
        self.latticemap = latticemap
        # if check:
        #     # print("checking", self.domain_cone, self.codomain_cone, self.latticemap)
        #     assert latticemap in Hom(domain_cone.lattice(), codomain_cone.lattice())
        #     fm = FanMorphism(latticemap, Fan([domain_cone]), codomain = Fan([codomain_cone]))

    def compose(self, CM, check=True):
        r"""
        Composing two fan morphisms.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import AbstractFanMorphism
            sage: F3 = Fan([Cone([[0,1,0], [1,0,1]])])
            sage: F2 = Fan([Cone([[0,1],[1,0]])])
            sage: F1 = Fan([Cone([[1]])])
            sage: FM32 = AbstractFanMorphism(Hom(F3.lattice(), F2.lattice())([[1,0],[0,1],[0,0]]), F3, F2)
            sage: FM21 = AbstractFanMorphism(Hom(F2.lattice(), F1.lattice())([[1],[0]]), F2, F1)
            sage: FM31 = AbstractFanMorphism(Hom(F3.lattice(), F1.lattice())([[1],[0], [0]]), F3, F1)
            sage: FM21.compose(FM32, check = True) == FM31
            True
            """
        assert self.domain_fan() == CM.codomain_fan()
        return AbstractFanMorphism(self.latticemap * CM.latticemap, CM.domain_fan(), self.codomain_fan(), check=check)

    def __eq__(self, other):
        if not self.domain_fan() == other.domain_fan():
            return False
        if not self.codomain_fan() == other.codomain_fan():
            return False
        for R in self.domain_fan().rays():
            if not self.latticemap(R) == other.latticemap(R):
                return False
        return True

    def domain(self):
        r"""
        Domain of fan morphisms.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import AbstractFanMorphism
            sage: F1 = Fan([Cone([[0,1],[1,0]])])
            sage: F2 = Fan([Cone([[1]])])
            sage: FM12 = AbstractFanMorphism(Hom(F1.lattice(), F2.lattice())([[1],[0]]), F1, F2)
            sage: FM12.domain() == F1
            True
            """
        return self.domain_fan

    def codomain(self):
        r"""
        Domain of fan morphisms.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import AbstractFanMorphism
            sage: F1 = Fan([Cone([[0,1],[1,0]])])
            sage: F2 = Fan([Cone([[1]])])
            sage: FM12 = AbstractFanMorphism(Hom(F1.lattice(), F2.lattice())([[1],[0]]), F1, F2)
            sage: FM12.domain() == F1
            True
            """
        return self.codomain_fan


class StackyFan(FiniteCat):
    r"""
    A cone stack, specified by a list of objects, a dictionary sending each
    object to a fan, and facemaps encoding how each morphism between objects
    corresponds to a face map between the associated fans.

    NOTE:
    Currently this is a lazy implementation, with objects giving the maximal
    fans of the stacky fan, and facemaps being trivial. In other words, the
    current code does not check compatibility on overlaps.
    """
    # There are things to decide here. The cone list should probably be complete, but how complete do we want the list of facemaps to be? Should it include all the identities on cones? Is it enough just to include maps between cones of consecutive (or equal) dimensions?
    # Lemma: Let C be a strictly convex rational polyhedral cone, and F a face of C. Then there exists a chain of faces F = F_0 \sub F_1 \sub \cdots \sub F_n = C such that each F_i is a face of F_{i + 1} and dim F_i + 1 = dim(F_{i + 1}).
    # Proof. Follows from the fact that maximal faces have dimension 1 less than that of C, and that every proper face is a face of a maximal face. \qed
    # So I think perhaps we just want to include the maps which go between cones of consecutive or equal dimensions.
    # Or maybe the structure should be a digraph? But then we still have to decide whether we want alll arrows or just a generating set. Is there a nice equivalence on digraphs?
    def __init__(self, fans, facemaps):
        r"""
        fans : dictionary sending objects to fans
        facemaps: dictionary sending morphisms between objects to morphisms between fans
        """
        FiniteCat.__init__(self, list(fans), facemaps)
        self._dim = max(f.dim() for f in fans.values())
        self._fandict = fans
        self._facemapdict = facemaps
    # def __repr__(self):
    #     return "Stacky fan built from " + str(len(self.objects())) + " fans and some face maps. "

    def __eq__(self, other):
        return self._fandict == other._fandict and self._facemapdict == other._facemapdict

    def is_face_of(self, C1, C2):
        r"""here C1, C2 are cones, and this function will check whether C1 is a face of C2. If all facemaps are given this is fast, but if only a generating set is given then there is something to check. """
        return NotImplementedError

    def list_cones(self):
        return NotImplementedError

    def fan(self, ob):
        return self._fandict[ob]

    def fans(self, codim=None):
        if codim is None:
            return list(self._fandict.values())
        else:
            return [F for F in self._fandict.values() if F.dim() == self._dim - codim]

    def maxfans(self):
        return self.fans(codim=0)

    def maxobjects(self):
        return [O for O in self._fandict if self._fandict[O].dim() == self._dim]

    def dim(self):
        return self._dim

    def refines(self, other):
        # TODO: ignores facemaps
        return set(self.objects) == set(other.objects) and all(self.fan(ob).refines(other.fan(ob)) for ob in self.objects)

    def common_refinement(self, other):
        r"""
        Computes the common refinement of self and other, assuming that the underlying objects agree.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import StackyFan
            sage: F11 = Fan([Cone([(1,0),(0,1)]), Cone([(1,0),(0,-1)])])
            sage: F12 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1),(0,-1)])])
            sage: F21 = Fan([Cone([(0,0,1),(1,0,1),(0,1,1),(1,1,1)])])
            sage: F22 = F21.make_simplicial()
            sage: SF1 = StackyFan({0:F11, 's':F21},[])
            sage: SF2 = StackyFan({0:F12, 's':F22},[])
            sage: SF = SF1.common_refinement(SF2)
            sage: sorted(SF.fan(0).rays())
            [N(0, -1), N(0, 1), N(1, 0), N(1, 1)]
            sage: SF.fan('s')==F22
            True
        """
        return StackyFan({ob: f.common_refinement(other._fandict[ob]) for ob, f in self._fandict.items()}, self._facemapdict)  # TODO: ignores facemaps

    def __hash__(self):
        return hash(tuple(sorted(self._fandict.items())))


def stackify_fan(fan, check=True):
    # Did = End(fan.lattice()).one()
    return StackyFan([fan], [])  # [AbstractFanMorphism(Did, fan, fan)])
    # FCL = fan.cone_lattice()#this is terribly slow! Can we do something faster? Is it always enough to check inclusions of cones of consecutive dimensions?
    # mylist = [a for a in FCL.list()]
    # del mylist[-1]
    # long_morphism_list = list(FCL.chains())
    # del long_morphism_list[0]
    # morphism_list = [a for a in long_morphism_list if not a[-1] == fan]
    # # print([[P[0], P[1], Hom(P[0].lattice(), P[1].lattice())] for P in morphism_list][0])
    # facemaps = [ConeMorphism(P[0], P[-1], Hom(P[0].lattice(), P[-1].lattice())(1), check = check) for P in morphism_list]
    # return StackyFan(mylist, facemaps)


try:
    positive_orthant = cones.nonnegative_orthant
except NameError:
    def positive_orthant(n):
        return Cone([[0] * i + [1] + [0] * (n - 1 - i) for i in range(n)])


class ModuliStableTropicalCurves(StackyFan):
    r"""
    The StackyFan given by the moduli space of tropical curves of
    genus g with n legs/unbounded ends.
    """
    def __init__(self, g, n, lazy=True):
        self._g = g
        self._n = n
        self._DG = degeneration_graph(g, n)
        if lazy:
            self._maxgraphs = [l[0] for l in self._DG[0][3 * g - 3 + n]]
            StackyFan.__init__(
                self, {m: Fan([positive_orthant(3 * g - 3 + n)]) for m in self._maxgraphs}, [])
        else:
            raise NotImplementedError('only lazy Mgntrop implemented')
            # self.strata = [[b[0] for b in a] for a in self.DG[0]]
            # self.fanlist = []
            # for S in self.strata:
            #     self.fanlist.append(Fan([positive_orthant(S.num_edges())]))
            # StackyFan.__init__(self, {}, [])
            # I think this needs more thought. On the one hand StackyFan just wants the objects of the category fo be fans. On the other hand, for what we do here it is really important to remember the isomoprhism between the ambient lattive and the free group generated by edges. Should think about the best way to set this up. Maybe a StackyFan together with a dictionary whose keys are stablegraphs and whose elements are the isomoprhisms between the lattice of the standard fan and the free group on the edges? But this is just the same data as an ordering of the edges, which is actually given to us by the graph! Hmmm...
            # I think that by fixing a representative of each stable graph with degeneration_graph, we do have a canonical ordering on the edges
            # So in principle I think all the data needed is there, but probably we should write functions to conveniently access this data.

    def DRPicGraph(self, Avector, gamma):
        r"""
        Return the PicGraph on gamma with weights induced by the line bundle
        omega_log^k(-sum_i a_i p_i), as it appears in the cycle DR_g(Avector).

        EXAMPLES::

            sage: from admcycles import StableGraph
            sage: from admcycles.logtaut.picgraph import ModuliStableTropicalCurves
            sage: M = ModuliStableTropicalCurves(2,3)
            sage: M.DRPicGraph([-1,6,5],StableGraph([1,1],[[1,2,4],[3,5]],[(4,5)]))
            [(1, 1), (1, -1)] [[1, 2, 4], [3, 5]] [(4, 5)]
        """
        g, n = self._g, self._n
        k = ZZ(sum(Avector) / ZZ(2 * g - 2 + n))
        weights = [k * (2 * gi - 2 + gamma.num_legs(i)) - sum(Avector[j - 1]
                                                              for j in gamma.list_markings(i)) for i, gi in enumerate(gamma.genera())]
        return PicGraph(gamma.genera(), gamma.legs(), gamma.edges(), weights)

    def is_small(self, phi):
        r"""
        Checks whether the stability condition phi is small.
        This guarantees that the output of the logDR function is actually
        logDR, rather than differing from it by some wall-crossing.

        EXAMPLES::
            sage: from admcycles.logtaut.picgraph import GraphStabilitySpace, ModuliStableTropicalCurves
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):0.3, (1,):0.1, (2,):-0.1})
            sage: M = ModuliStableTropicalCurves(1,2)
            sage: M.is_small(phi)
            True
            sage: phi2 = V(0,{(0,(1,2)):3.3, (1,):0.1, (2,):-0.1})
            sage: M.is_small(phi2)
            False
        """
        zvec = [0 for a in range(self._n)]
        for gamma in self._maxgraphs:
            Pgamma = self.DRPicGraph(zvec, gamma)
            if not Pgamma.is_quasistable(phi):
                return False
        return True

    def second_PL_function(self, Avector, phi):
        r"""
        Return second PL function from logDR formula with respect to stability
        condition phi.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import GraphStabilitySpace, ModuliStableTropicalCurves
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):0.3, (1,):0.1, (2,):-0.1})
            sage: M = ModuliStableTropicalCurves(1,2)
            sage: M.second_PL_function([3,-3], phi)
            Piecewise polynomial on Finite category with 2 objects.
            [0, 0] [[1, 2, 3], [4, 5, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : 0
            [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 2), N(1, 3)) : -6*x0
            (N(1, 1), N(2, 1)) : -2*x0 - 2*x1
            (N(1, 0), N(3, 1)) : -6*x1
            (N(0, 1), N(1, 3)) : -6*x0
            (N(1, 1), N(1, 2)) : -2*x0 - 2*x1
            (N(2, 1), N(3, 1)) : -6*x1
        """
        polydict = {}
        for gamma in self._maxgraphs:
            Pgamma = self.DRPicGraph(Avector, gamma)
            polydict[gamma] = Pgamma.second_PL_function(phi)
        SF = StackyFan({gamma: poly.parent()._fan for gamma,
                       poly in polydict.items()}, [])
        R = PiecewisePolynomialRing(SF)
        return R(polydict)

    def first_PP_function(self, Avector, dmax, phi):
        r"""
        Return first PP function from logDR formula with respect to stability
        condition phi, up to degree dmax.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import GraphStabilitySpace, ModuliStableTropicalCurves
            sage: V = GraphStabilitySpace(1,2)
            sage: phi = V(0,{(0,(1,2)):0.3, (1,):0.1, (2,):-0.1})
            sage: M = ModuliStableTropicalCurves(1,2)
            sage: M.first_PP_function([3,-3], 1, phi)
            Piecewise polynomial on Finite category with 2 objects.
            [0, 0] [[1, 2, 3], [4, 5, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : -1/12*x1 + 1
            [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 2), N(1, 3)) : -1/12*x0 - 1/12*x1 + 1
            (N(1, 1), N(2, 1)) : -1/12*x0 - 1/12*x1 + 1
            (N(1, 0), N(3, 1)) : -1/12*x0 - 1/12*x1 + 1
            (N(0, 1), N(1, 3)) : -1/12*x0 - 1/12*x1 + 1
            (N(1, 1), N(1, 2)) : -1/12*x0 - 1/12*x1 + 1
            (N(2, 1), N(3, 1)) : -1/12*x0 - 1/12*x1 + 1
        """
        polydict = {}
        for gamma in self._maxgraphs:
            Pgamma = self.DRPicGraph(Avector, gamma)
            polydict[gamma] = Pgamma.first_PP_function(dmax, phi)
        SF = StackyFan({gamma: poly.parent()._fan for gamma,
                       poly in polydict.items()}, [])
        R = PiecewisePolynomialRing(SF)
        return R(polydict)

    def maximal_graph_containing(self, gamma):
        r"""
        Given a stable graph gamma, return a tuple (Gamma, dicv, dicl, dice) of a 3-valent
        graph Gamma and maps dicv: V(Gamma) -> V(gamma), dicl: H(gamma) -> H(Gamma),
        dice: E(gamma) -> E(Gamma) writing Gamma as a specialization of gamma.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import ModuliStableTropicalCurves
            sage: from admcycles import StableGraph
            sage: M = ModuliStableTropicalCurves(0,5)
            sage: gamma = StableGraph([0,0],[[1,3,5,6],[2,4,9]],[(9,6)])
            sage: gamma2 = StableGraph([0, 0, 0], [[1, 6, 8], [2, 4, 7], [3, 5, 9]], [(6, 7), (8, 9)])
            sage: MG = [gamma2,{0: 0, 1: 1, 2: 0}, {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 9: 7}, {(9, 6): (6, 7)}]
            sage: list(M.maximal_graph_containing(gamma)) == MG
            True
        """
        g, n = (self._g, self._n)
        r, i, _, dicl = deggrfind(gamma)
        dicl = {i: dicl[i] for i in dicl if i > n}  # removing the markings
        for e in range(gamma.num_edges(), 3 * g - 3 + n):
            he, mor = self._DG[0][r][i][1], self._DG[0][r][i][3]
            mor = next(iter(mor))
            hetarget = self._DG[0][r + 1][mor[0]][1]
            diclnew = {a: hetarget[b] for a, b in zip(he, mor[1])}
            dicl = {a: diclnew[dicl[a]] for a in dicl}
            r += 1
            i = mor[0]
        # putting markings back in
        dicl.update({i: i for i in range(1, n + 1)})
        Gamma = self._DG[0][r][i][0]
        dicv = dicv_reconstruct(Gamma, gamma, dicl)
        dice = {}
        for e in gamma.edges():
            ep = (dicl[e[0]], dicl[e[1]])
            if ep in Gamma.edges():
                dice[e] = ep
            else:
                dice[e] = (ep[1], ep[0])
        return (Gamma, dicv, dicl, dice)

    def __repr__(self):
        return "Moduli of stable tropical curves of genus {} with {} marked points.".format(self._g, self._n)

    def tautological_class(self, f, d=None):
        r"""
        Converts the piecewise polynomial function f on Mgntrop (with coefficients in
        the tautological ring) into a TautologicalClass. If d is given, only compute
        codimension d (with respect to piecewise polynomial) part of the class.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import ModuliStableTropicalCurves, PiecewisePolynomialRing, positive_orthant
            sage: M = ModuliStableTropicalCurves(1,2)
            sage: L = M._maxgraphs; L
            [[0, 0] [[1, 2, 3], [4, 5, 6]] [(3, 4), (5, 6)],
             [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)]]
            sage: R = PiecewisePolynomialRing(M)
            sage: S = PiecewisePolynomialRing(Fan([positive_orthant(2)]))
            sage: tau = S.phi(Cone([(1,0)]))
            sage: f = R({L[0]: tau, L[1]: S.zero()})
            sage: f
            Piecewise polynomial on Moduli of stable tropical curves of genus 1 with 2 marked points.
            [0, 0] [[1, 2, 3], [4, 5, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : x0
            [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : 0
            sage: t = M.tautological_class(f); t
            Graph :      [0, 1] [[1, 2, 4], [5]] [(4, 5)]
            Polynomial : 1
            sage: t = M.tautological_class(f**2); t
            Graph :      [0, 1] [[1, 2, 4], [5]] [(4, 5)]
            Polynomial : -psi_5
            sage: tau2 = S.phi(Cone([(0,1)]))
            sage: g = R({L[0]: tau2, L[1]: tau+tau2})
            sage: g
            Piecewise polynomial on Moduli of stable tropical curves of genus 1 with 2 marked points.
            [0, 0] [[1, 2, 3], [4, 5, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : x1
            [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : x0 + x1
            sage: s = M.tautological_class(g); s
            Graph :      [0] [[4, 5, 1, 2]] [(4, 5)]
            Polynomial : 1/2
            sage: s2 = M.tautological_class(g**2); s2
            Graph :      [0] [[4, 5, 1, 2]] [(4, 5)]
            Polynomial : -psi_4
            <BLANKLINE>
            Graph :      [0, 0] [[5, 2, 7], [6, 1, 8]] [(5, 6), (7, 8)]
            Polynomial : 1
            sage: (s**2-s2).simplify()
            0
        """
        g, n = (self._g, self._n)
        R = TautologicalRing(g, n)
        if d is None:
            return sum((self.tautological_class(f, e) for e in range(3 * g - 3 + n + 1)), R.zero())
        result = R.zero()
        for t in R.generators(d):
            ds = list(t._terms.values())[0]
            # if t contains kappa-classes, or psi at markings, it cannot appear
            kap, psi, _ = ds.poly[0]
            gamma = ds.gamma
            if any(k for k in kap) or any(i <= n for i in psi):
                continue
            # now we know ds is a StableGraph, decorated with powers of psi-classes at half-edges
            # look up maximal cone that can tell us the correct coefficient
            Gamma, _, _, dice = self.maximal_graph_containing(gamma)
            R = f.parent().polynomial_ring()
            x = R.gens()
            E = Gamma.edges()
            # compute the monomial which is the product of x_e ^ d_e where e runs through
            # edges of Gamma coming from gamma, and d_e = d_1 + d_2 + 1 is the correct degree to give us
            # the decoration psi_h^d_1 psi_h'^d_2 on the edge e
            mono = prod((x[E.index(dice[e])]**(psi.get(e[0], 0) +
                        psi.get(e[1], 0) + 1) for e in gamma.edges()), R.one())
            # C is coefficient of mono in the correct monomial
            C = f.polynomial((Gamma, positive_orthant(3 * g - 3 + n)))[mono]
            # Now you just have to stick together the correct binomial factors, signs, automorphism factors etc.
            scalar_factor = ZZ.one() / ds.automorphism_number()
            scalar_factor *= prod(((-1)**(psi.get(e[0], 0) + psi.get(e[1], 0))) * binomial(
                psi.get(e[0], 0) + psi.get(e[1], 0), psi.get(e[0], 0)) for e in gamma.edges())
            result += scalar_factor * C * t
        return result


_CommutativeRings = Rings().Commutative()


def multi_truncate(poly, d):
    r"""
    Truncates a multivariate polynomial to degree at most d.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import *
        sage: R.<a,b> = PolynomialRing(QQ)
        sage: f = a^3 + a*b + b^2 - a + 16
        sage: multi_truncate(f, 2)
        a*b + b^2 - a + 16
        sage: multi_truncate(f, 1)
        -a + 16
        sage: multi_truncate(f, 3)
        a^3 + a*b + b^2 - a + 16
    """
    R = poly.parent()
    di = poly.dict()
    return R({k: c for k, c in di.items() if sum(k) <= d})

# NOTE: replaces admcycles.tautclass


class PiecewisePolynomial(ModuleElement):
    r"""
    A piecewise polynomial function on a (possibly stacky) fan.
    """

    def __init__(self, parent, polydict):
        r"""
        INPUT:

        parent: a class:`TautologicalRing`
        polydict: a dictionary:

        - sending cones to polynomials for non-stacky fans
        - sending objects to PiecewisePolynomials on their associated fan, for stacky fans
        """
        ModuleElement.__init__(self, parent)

        if isinstance(polydict, dict):
            # Convert keys of polydict from possibly abstract cones into Cone_of_fan objects
            # TODO: can this be avoided?
            P = self.parent()
            f = P._fan
            if not self.parent().is_stacky():  # and not all(isinstance(c,Cone_of_fan) for c in polydict)
                polydict = {Cone_of_fan(f, [i for i, r in enumerate(f.rays()) if r in c.rays(
                )]): polydict[c] for c in polydict}  # f.cone_containing(c)
            self._polydict = polydict
            # TODO: implement check option for parent ring and check procedure
        else:
            raise TypeError

    def get_fan(self):
        return self.parent()._fan

    def polynomial(self, c):
        r"""
        Return polynomial on c, where c is a cone (for non-stacky fans) or a tuple (ob, c) ...

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F = Fan([Cone([(1,0),(0,1)]), Cone([(1,0), (0,-1)])])
            sage: R = PiecewisePolynomialRing(F)
            sage: S = R.polynomial_ring()
            sage: x0, x1 = S.gens()
            sage: di = {Cone([(1,0),(0,1)]) : x0, Cone([(1,0),(0,-1)]): x0+x1}
            sage: a = R(di)
            sage: a.polynomial(Cone([(1,0),(0,1)]))
            x0
            sage: a.polynomial(Cone([(0,1),(1,0)]))
            x0
            sage: a.polynomial(F.cone_containing((1,-1)))
            x0 + x1
        """
        P = self.parent()
        if not P.is_stacky():
            try:
                return self._polydict[c]
            except KeyError:
                F = P._fan
                return self._polydict[F.cone_containing(c)]
        else:
            return self._polydict[c[0]].polynomial(c[1])

    def copy(self):
        P = self.parent()
        return P.element_class(P, {g: copy(t) for g, t in self._polydict.items()})

    def _repr_(self):
        s = 'Piecewise polynomial on ' + repr(self.parent()._fan) + '\n'
        if not self.parent()._stacky:
            s += '\n'.join(repr(tuple(sorted(g.rays()))) + ' : ' + repr(t)
                           for g, t in self._polydict.items())
        else:
            s += '\n'.join(repr(g) + ' :\n' + repr(t)
                           for g, t in self._polydict.items())
        return s

    def _unicode_art_(self):
        r"""
        Return unicode art for the piecewise polynomial.
        """
        from sage.typeset.unicode_art import unicode_art, UnicodeArt
        s = unicode_art('Piecewise polynomial on ' + repr(self.parent()._fan))
        if not self.parent()._stacky:
            s *= prod([unicode_art(repr(tuple(g.rays())) + ' : ' + repr(t))
                      for g, t in self._polydict.items()], UnicodeArt())
        else:
            s *= prod([g._unicode_art_() * unicode_art(repr(t))
                      for g, t in self._polydict.items()], UnicodeArt())
        return s

    def truncate(self, d):
        r"""
        Truncate polynomials to degree at most d on all cones.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1), (0,-1)])])
            sage: C11 = Cone([(1,1),(0,1)]); C12 = Cone([(1,1),(0,-1)])
            sage: F1 = Fan([C11, C12])
            sage: R.<x0,x1> = PolynomialRing(QQ,2)
            sage: D1 = {C11: 2*x0*x1^2 + x0 + 5, C12: x0^3+x1^3 + x1 + 5}
            sage: a = PiecewisePolynomialRing(F1)(D1)
            sage: a.truncate(2)
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : x0 + 5
            (N(0, -1), N(1, 1)) : x1 + 5
        """
        P = self.parent()
        if not P._stacky:
            return P.element_class(P, {g: multi_truncate(term, d) for g, term in self._polydict.items()})
        else:
            return P.element_class(P, {g: term.truncate(d) for g, term in self._polydict.items()})

    def __neg__(self):
        r"""
        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1), (0,-1)])])
            sage: C11 = Cone([(1,1),(0,1)]); C12 = Cone([(1,1),(0,-1)])
            sage: F1 = Fan([C11, C12])
            sage: R.<x0,x1> = PolynomialRing(QQ,2)
            sage: D1 = {C11: 2*x0, C12: x0+x1}
            sage: a = PiecewisePolynomialRing(F1)(D1)
            sage: -a
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : -2*x0
            (N(0, -1), N(1, 1)) : -x0 - x1
        """
        P = self.parent()
        return P.element_class(P, {g: -term for g, term in self._polydict.items()})

    def _add_(self, other):
        r"""
        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: C11 = Cone([(1,1),(0,1)]); C12 = Cone([(1,1),(0,-1)])
            sage: C21 = Cone([(1,-1),(0,1)]); C22 = Cone([(1,-1),(0,-1)])
            sage: F1 = Fan([C11, C12])
            sage: F2 = Fan([C21, C22])
            sage: R.<x0,x1> = PolynomialRing(QQ,2)
            sage: D1 = {C11: 2*x0, C12: x0+x1}
            sage: D2 = {C21: x0**2, C22: x1**2}
            sage: a = PiecewisePolynomialRing(F1)(D1)
            sage: b = PiecewisePolynomialRing(F2)(D2)
            sage: a+b
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : x0^2 + 2*x0
            (N(1, -1), N(1, 1)) : x0^2 + x0 + x1
            (N(0, -1), N(1, -1)) : x1^2 + x0 + x1

            sage: from admcycles.logtaut.picgraph import StackyFan, PiecewisePolynomialRing
            sage: F11 = Fan([Cone([(1,0),(0,1)]), Cone([(1,0),(0,-1)])])
            sage: F12 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1),(0,-1)])])
            sage: SF1 = StackyFan({0:F11},[])
            sage: SF2 = StackyFan({0:F12},[])
            sage: R1 = PiecewisePolynomialRing(SF1)
            sage: R2 = PiecewisePolynomialRing(SF2)
            sage: R11 = PiecewisePolynomialRing(F11)
            sage: R12 = PiecewisePolynomialRing(F12)
            sage: a = R11.phi(Cone([(1,0)]))
            sage: b = R12.phi(Cone([(0,1)]))
            sage: A = R1({0:a})
            sage: B = R2({0:b})
            sage: A+B
            Piecewise polynomial on Finite category with 1 objects.
            0 :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : x1
            (N(1, 0), N(1, 1)) : x0
            (N(0, -1), N(1, 0)) : x0
        """
        P = self.parent()
        Q = other.parent()
        if P._fan == Q._fan:
            return P.element_class(P, {c: self._polydict[c] + other._polydict[c] for c in self._polydict})
        raise ValueError('functions live on different fans')

    def _lmul_(self, other):
        r"""
        Scalar multiplication by ``other``.

        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1), (0,-1)])])
            sage: C11 = Cone([(1,1),(0,1)]); C12 = Cone([(1,1),(0,-1)])
            sage: F1 = Fan([C11, C12])
            sage: R.<x0,x1> = PolynomialRing(QQ,2)
            sage: D1 = {C11: 2*x0, C12: x0+x1}
            sage: a = PiecewisePolynomialRing(F1)(D1)
            sage: 3*a
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : 6*x0
            (N(0, -1), N(1, 1)) : 3*x0 + 3*x1
        """
        P = self.parent()
        assert parent(other) is self.base_ring()
        if other.is_zero():
            return P.zero()
        elif other.is_one():
            # TODO: if elements were immutable we could return self
            return self.copy()
        new_terms = {g: other * term for g, term in self._polydict.items()}
        return P.element_class(P, new_terms)

    def _mul_(self, other):
        r"""
        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1), (0,-1)])])
            sage: F2 = Fan([Cone([(1,-1),(0,1)]), Cone([(1,-1), (0,-1)])])
            sage: C11 = Cone([(1,1),(0,1)]); C12 = Cone([(1,1),(0,-1)])
            sage: C21 = Cone([(1,-1),(0,1)]); C22 = Cone([(1,-1),(0,-1)])
            sage: F1 = Fan([C11, C12])
            sage: F2 = Fan([C21, C22])
            sage: R.<x0,x1> = PolynomialRing(QQ,2)
            sage: D1 = {C11: 2*x0, C12: x0+x1}
            sage: D2 = {C21: x0**2, C22: x1**2}
            sage: a = PiecewisePolynomialRing(F1)(D1)
            sage: b = PiecewisePolynomialRing(F2)(D2)
            sage: a*b
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : 2*x0^3
            (N(1, -1), N(1, 1)) : x0^3 + x0^2*x1
            (N(0, -1), N(1, -1)) : x0*x1^2 + x1^3

            sage: from admcycles.logtaut.picgraph import StackyFan, PiecewisePolynomialRing
            sage: F11 = Fan([Cone([(1,0),(0,1)]), Cone([(1,0),(0,-1)])])
            sage: F12 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1),(0,-1)])])
            sage: SF1 = StackyFan({0:F11},[])
            sage: SF2 = StackyFan({0:F12},[])
            sage: R1 = PiecewisePolynomialRing(SF1)
            sage: R2 = PiecewisePolynomialRing(SF2)
            sage: R11 = PiecewisePolynomialRing(F11)
            sage: R12 = PiecewisePolynomialRing(F12)
            sage: a = R11.phi(Cone([(1,0)]))
            sage: b = R12.phi(Cone([(0,1)]))
            sage: A = R1({0:a})
            sage: B = R2({0:b})
            sage: A*B
            Piecewise polynomial on Finite category with 1 objects.
            0 :
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : -x0^2 + x0*x1
            (N(1, 0), N(1, 1)) : 0
            (N(0, -1), N(1, 0)) : 0
        """
        P = self.parent()
        Q = other.parent()
        if P._fan == Q._fan:
            return P.element_class(P, {c: self._polydict[c] * other._polydict[c] for c in self._polydict})
        raise ValueError('functions live on different fans')

    def degree_part(self, d):
        r"""
        Return the homogeneous component of degree ``d``.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,1),(0,1)]), Cone([(1,1), (0,-1)])])
            sage: C11 = Cone([(1,1),(0,1)]); C12 = Cone([(1,1),(0,-1)])
            sage: F1 = Fan([C11, C12])
            sage: R.<x0,x1> = PolynomialRing(QQ,2)
            sage: D1 = {C11: 3+2*x0+x0^2-5*x0*x1, C12: 3+x0+x1-4*x1^2}
            sage: a = PiecewisePolynomialRing(F1)(D1)
            sage: a.degree_part(2)
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 1)) : x0^2 - 5*x0*x1
            (N(0, -1), N(1, 1)) : -4*x1^2
        """
        P = self.parent()
        D = {}
        for g, term in self._polydict.items():
            if hasattr(term, "_polydict"):
                D[g] = term.degree_part(d)
            else:
                D[g] = term.parent()(0)
                for coeff, monom in term:
                    if monom.degree() == d:
                        D[g] += coeff * monom
        return P.element_class(P, D)

    def pushforward(self, F):
        r"""
        Computes the pushforward of self to a (stacky) fan F that is coarser than the underlying
        fan of self. In stacky case: assume that objects of fan of self and of F agree.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: R = PiecewisePolynomialRing(F1)
            sage: tau = R.phi(Cone([(1,1)]))
            sage: F2 = Fan([Cone([(1,0),(0,1)])])
            sage: tau.pushforward(F2)  # pi_*(E) = 0
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : 0
            sage: (tau**2).pushforward(F2) # pi_*(E^2) = - [pt]
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : -x0*x1

        No longer gives errors for pushforwards in non-simplicial fans::

            sage: T = Fan([Cone([(1,0),(2,1)]), Cone([(2,1),(0,1)])])
            sage: Tr = PiecewisePolynomialRing(T)
            sage: Tr.one().pushforward(Fan([Cone([(1,0),(0,1)])]))
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(0, 1), N(1, 0)) : 1
        """
        P = self.parent()
        Q = PiecewisePolynomialRing(F, base_ring=P.base_ring())
        if not P.is_stacky():
            S = Q.polynomial_ring()
            dic, dicinv = refinedicts(P._fan, F)
            polydict = {C: S(Q.phiinverse(C)**(-1) * sum(P.phiinverse(D) *
                             self.polynomial(D) for D in dicinv[C])) for C in F.cones(codim=0)}
        else:
            polydict = {ob: f.pushforward(F.fan(ob))
                        for ob, f in self._polydict.items()}
        return Q(polydict)

    def _richcmp_(self, other, op):
        r"""
        Implementation of comparisons (``==`` and ``!=``).

        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F = FaceFan(lattice_polytope.cross_polytope(3))
            sage: R = PiecewisePolynomialRing(F)
            sage: a = R.one()
            sage: b = R.zero()
            sage: a == b
            False
            sage: b == 0*a
            True
        """
        if op != op_EQ and op != op_NE:
            raise TypeError('incomparable')

        equal = (self.parent() == other.parent()) and (
            self._polydict == other._polydict)
        return equal == (op == op_EQ)

    def __bool__(self):
        r"""
        Translates function to bool value (True if nonzero, False if zero).

        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F = FaceFan(lattice_polytope.cross_polytope(3))
            sage: R = PiecewisePolynomialRing(F)
            sage: bool(R.one())
            True
            sage: bool(R.zero())
            False
        """
        return not all(b.is_zero() for b in self._polydict.values())

    # Python 2 support
    __nonzero__ = __bool__

    def is_zero(self):
        r"""
        Checks whether function is zero.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F = FaceFan(lattice_polytope.cross_polytope(3))
            sage: R = PiecewisePolynomialRing(F)
            sage: R.one().is_zero()
            False
            sage: R.zero().is_zero()
            True
        """
        return not self


class PiecewisePolynomialRing(UniqueRepresentation, Parent):
    r"""
    The ring of piecewise polynomial functions on some (stacky) fan.
    """
    Element = PiecewisePolynomial

    @staticmethod
    def __classcall__(cls, *args, **kwds):
        fan = kwds.pop('fan', None)
        base_ring = kwds.pop('base_ring', None)
        if kwds:
            raise ValueError('unknown arguments {}'.format(list(kwds)))
        if len(args) >= 1:
            if fan is not None:
                raise ValueError('fan specified twice')
            fan = args[0]
        if len(args) >= 2:
            # fan and base ring
            if base_ring is not None:
                raise ValueError('base ring specified twice')
            base_ring = args[1]
        if len(args) > 2:
            raise ValueError(
                'too many arguments for PiecewisePolynomialRing: {}'.format(args))

        if base_ring is None:
            base_ring = QQ
        elif base_ring not in _CommutativeRings:
            raise ValueError(
                'base_ring (={}) must be a commutative ring'.format(base_ring))

        return super(PiecewisePolynomialRing, cls).__classcall__(cls, fan, base_ring)

    def __init__(self, fan, base_ring):
        r"""
        INPUT:

        fan : either a SageMath RationalPolyhedralFan or a StackyFan
          genus
        base_ring : ring
          base ring
        """
        Parent.__init__(self, base=base_ring,
                        category=Algebras(base_ring).Commutative())
        self._fan = fan
        self._phiinvdict = None
        if isinstance(fan, sage.geometry.fan.RationalPolyhedralFan):
            self._stacky = False
        elif isinstance(fan, StackyFan):
            self._stacky = True
        else:
            raise ValueError('fan must be RationalPolyhedralFan or StackyFan')

    def polynomial_ring(self, ob=None):
        r"""
        The polynomial ring containing the individual polynomials on cones (with respect to common
        lattice in case of non-stacky fans).
        If ob is given (in stacky case) instead return PiecewisePolynomialRing on fan associated to ob.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import *
            sage: F = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: S = PiecewisePolynomialRing(F)
            sage: S.polynomial_ring()
            Multivariate Polynomial Ring in x0, x1 over Rational Field
        """
        if not self.is_stacky() or ob is None:
            return PolynomialRing(self.base_ring(), self._fan.dim(), 'x')
        return PiecewisePolynomialRing(self._fan.fan(ob), base_ring=self.base_ring())

    def is_stacky(self):
        r"""
        Returns whether the underlying fan is stacky.
        """
        return self._stacky

    def is_integral_domain(self):
        r"""
        Checks if ring is integral domain.
        """
        raise NotImplementedError

    def is_field(self):
        r"""
        Checks if ring is field.
        """
        raise NotImplementedError

    def is_prime_field(self):
        r"""
        Checks if ring is prime field.
        """
        raise NotImplementedError

    def _coerce_map_from_(self, other):
        r"""
        TESTS::

            sage: from admcycles.logtaut import PiecewisePolynomialRing
            sage: F = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: PiecewisePolynomialRing(F, base_ring=QQ['x','y']).has_coerce_map_from(PiecewisePolynomialRing(F, base_ring=QQ))
            True
        """
        if isinstance(other, PiecewisePolynomialRing) and \
           self._fan.refines(other._fan) and \
           self.base_ring().has_coerce_map_from(other.base_ring()):
            return True

    def construction(self):
        r"""
        Return a functorial construction (when applied to a base ring).

        This function is mostly intended to make the piecewise polynomial
        ring behave nicely with the Sage ecosystem.

        EXAMPLES::

            sage: from admcycles.logtaut import PiecewisePolynomialRing
            sage: F = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: R = PiecewisePolynomialRing(F)
            sage: var('hbar')
            hbar
            sage: u = hbar * R.one(); u
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 0), N(1, 1)) : hbar
            (N(0, 1), N(1, 1)) : hbar
            sage: u.parent()
            PiecewisePolynomialRing(fan=Rational polyhedral fan in 2-d lattice N) over Symbolic Ring
        """
        return PiecewisePolynomialRingFunctor(self._fan), self.base_ring()

    def _repr_(self):
        return 'PiecewisePolynomialRing(fan={}) over {}'.format(self._fan, self.base_ring())

    def some_elements(self):
        r"""
        Return some elements in this piecewise polynomial ring.

        This is mostly used for sage test system.

        EXAMPLES::

            sage: from admcycles.logtaut import PiecewisePolynomialRing
            sage: F = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: _  = PiecewisePolynomialRing(F).some_elements()
        """
        base_elts = [self(s) for s in self.base_ring().some_elements()]
        elts = base_elts[:2] + base_elts[-2:]
        return elts

    # TODO: if immutable, we could cache the method
    # @cached_method
    def zero(self):
        r"""
        Return the zero element.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import ModuliStableTropicalCurves, PiecewisePolynomialRing
            sage: M = ModuliStableTropicalCurves(0,4)
            sage: R = PiecewisePolynomialRing(M)
            sage: R.zero()
            Piecewise polynomial on Moduli of stable tropical curves of genus 0 with 4 marked points.
            [0, 0] [[1, 2, 5], [3, 4, 6]] [(5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 1-d lattice N
            (N(1),) : 0
            [0, 0] [[1, 3, 5], [2, 4, 6]] [(5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 1-d lattice N
            (N(1),) : 0
            [0, 0] [[1, 4, 5], [2, 3, 6]] [(5, 6)] :
            Piecewise polynomial on Rational polyhedral fan in 1-d lattice N
            (N(1),) : 0
        """
        if not self.is_stacky():
            return self.element_class(self, {c: self.polynomial_ring().zero() for c in self._fan.cones(codim=0)})
        else:
            F = self._fan
            return self.element_class(self, {c: PiecewisePolynomialRing(F.fan(c), base_ring=self.base_ring()).zero() for c in F.objects})

    # TODO: if immutable, we could cache the method
    # @cached_method
    def one(self):
        r"""
        Return the fundamental class as a cohomology class.

        The fundamental class is the unit of the ring.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import ModuliStableTropicalCurves, PiecewisePolynomialRing
            sage: M = ModuliStableTropicalCurves(1,1)
            sage: R = PiecewisePolynomialRing(M)
            sage: R.one()
            Piecewise polynomial on Moduli of stable tropical curves of genus 1 with 1 marked points.
            [0] [[1, 2, 3]] [(2, 3)] :
            Piecewise polynomial on Rational polyhedral fan in 1-d lattice N
            (N(1),) : 1
            sage: R.one()**2 == R.one()
            True
        """
        if not self.is_stacky():
            return self.element_class(self, {c: self.polynomial_ring().one() for c in self._fan.cones(codim=0)})
        else:
            F = self._fan
            return self.element_class(self, {c: PiecewisePolynomialRing(F.fan(c), base_ring=self.base_ring()).one() for c in F.objects})

    def phi(self, C):
        r"""
        Return the piecewise linear function associated to the cone C of the underlying fan.

        For a simplicial fan and C a ray, it is supported on the star of C: it is the unique linear function
        equal to 1 on the generator of that ray and zero on all other rays of the fan. For C general,
        it is the product of phi_R for R in the rays of C.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: R = PiecewisePolynomialRing(F1)
            sage: R.phi(Cone([(1,1)]))
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 0), N(1, 1)) : x1
            (N(0, 1), N(1, 1)) : x0
            sage: R.phi(Cone([(1,0),(1,1)]))
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 0), N(1, 1)) : x0*x1 - x1^2
            (N(0, 1), N(1, 1)) : 0
        """
        if self.is_stacky() or not self._fan.is_simplicial():
            raise ValueError('phi only implemented for simplicial fans')
        if C.dim() == 1:
            rC = C.rays()[0]
            data = [[r, ZZ(r == rC)] for r in self._fan.rays()]
            return PL_from_fan_and_ray_values(self._fan, data)
            # CoF = self._fan.cone_containing(C)
            # starcones = CoF.star_generators()
        else:
            return C.span().index_in_saturation() * prod(self.phi(r) for r in C.faces(1))
            # return C.span().index_in(C.lattice())*prod(self.phi(r) for r in C.faces(1))

    def phiinverse(self, C):
        r"""
        Return the rational function phi_C^(-1) associated to the cone C of the
        underlying fan.

        EXAMPLES::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: R = PiecewisePolynomialRing(F1)
            sage: R.phiinverse(Cone([(1,0),(1,1)]))
            1/(x0*x1 - x1^2)
            sage: C = Cone([(0,0,1),(1,0,1),(0,1,1),(1,1,1)])
            sage: F2 = Fan([C])
            sage: R = PiecewisePolynomialRing(F2)
            sage: R.phiinverse(C)
            (-x2)/(-x0^2*x1^2 + x0^2*x1*x2 + x0*x1^2*x2 - x0*x1*x2^2)
            sage: F3=F2.subdivide(new_rays=[vector(QQ,(1,1,2))])
            sage: R = PiecewisePolynomialRing(F3)
            sage: sum(R.phiinverse(C) for C in F3.cones(codim=0)) # should agree with phiinverse for F2
            x2/(x0^2*x1^2 - x0^2*x1*x2 - x0*x1^2*x2 + x0*x1*x2^2)
        """
        if self.is_stacky():
            raise ValueError('phiinverse only implemented for non-stacky fans')
        # We compute phiinverse without reference to the fan for optimization
        # purposes.
        return cphiinv(C)

    def _element_constructor_(self, arg):
        r"""
        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing
            sage: F1 = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: F2 = Fan([Cone([(1,0),(0,1)])])
            sage: F2 = Fan([Cone([(1,0),(2,1)]), Cone([(2,1),(1,1)]), Cone([(1,1),(0,1)])])
            sage: R = PiecewisePolynomialRing(F1)
            sage: S = R.polynomial_ring()
            sage: x0, x1 = S.gens()
            sage: di = {Cone([(1,0),(1,1)]) : x0**2, Cone([(1,1),(0,1)]) : x0*x1}
            sage: a = R(di)
            sage: a
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 0), N(1, 1)) : x0^2
            (N(0, 1), N(1, 1)) : x0*x1
            sage: R2 = PiecewisePolynomialRing(F2)
            sage: b = R2(a)
            sage: b
            Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
            (N(1, 0), N(2, 1)) : x0^2
            (N(1, 1), N(2, 1)) : x0^2
            (N(0, 1), N(1, 1)) : x0*x1
        """
        if isinstance(arg, PiecewisePolynomial):
            P = arg.parent()
            if P == self:
                return self.element_class(self, {g: term.copy() for g, term in arg._polydict.items()})
            # if P.is_stacky() or self.is_stacky():
                # raise NotImplementedError
            if not self._fan.refines(P._fan):
                raise ValueError(
                    'can only create element from piecewise polynomial on coarser fan')
            if P.is_stacky():
                F = self._fan
                di = {c: self.polynomial_ring(c)(
                    arg._polydict[c]) for c in F.maxobjects()}
            else:
                di = {c: arg._polydict[P._fan.cone_containing(
                    sum(c.rays(), c.lattice().zero()))] for c in self._fan.cones(codim=0)}
            return self.element_class(self, di)
        elif isinstance(arg, dict):
            return self.element_class(self, arg)
        else:
            raise NotImplementedError(
                'unknown argument of type arg={}'.format(arg))


class PiecewisePolynomialRingFunctor(ConstructionFunctor):
    r"""
    Construction functor for piecewise polynomial ring.

    This class is the way to implement the "promotion of base ring" (see below in the examples).

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRing, PiecewisePolynomialRingFunctor
        sage: F = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
        sage: G = PiecewisePolynomialRingFunctor(F)
        sage: G(CC)
        PiecewisePolynomialRing(fan=Rational polyhedral fan in 2-d lattice N) over Complex Field with 53 bits of precision

        sage: var('hbar')
        hbar
        sage: (hbar^2 + 1) * PiecewisePolynomialRing(F).one()
        Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
        (N(1, 0), N(1, 1)) : hbar^2 + 1
        (N(0, 1), N(1, 1)) : hbar^2 + 1
    """
    rank = 10

    def __init__(self, fan):
        Functor.__init__(self, _CommutativeRings, _CommutativeRings)
        self._fan = fan

    def _repr_(self):
        r"""
        TESTS::

            sage: from admcycles.logtaut.picgraph import PiecewisePolynomialRingFunctor
            sage: F = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
            sage: G = PiecewisePolynomialRingFunctor(F); G  # indirect doctest
            PiecewisePolynomialRingFunctor(fan=Rational polyhedral fan in 2-d lattice N)
        """
        return 'PiecewisePolynomialRingFunctor(fan={})'.format(
            self._fan)

    def _apply_functor(self, R):
        return PiecewisePolynomialRing(self._fan, base_ring=R)

    def merge(self, other):
        r"""
        Return the merge of two piecewise polynomial ring functors.
        """
        if isinstance(other, PiecewisePolynomialRingFunctor):
            if self._fan == other._fan:
                return PiecewisePolynomialRingFunctor(self._fan)
            return PiecewisePolynomialRingFunctor(self._fan.common_refinement(other._fan))

    def __eq__(self, other):
        return isinstance(other, PiecewisePolynomialRingFunctor) and \
            self._fan == other._fan

    def __ne__(self, other):
        return not (self == other)

    __hash__ = ConstructionFunctor.__hash__


def logDR(g, Avector, d=None, phi=None, phiout=False):
    r"""
    Return the log double ramification cycle DR_g(Avector) as a mixed class
    up to codimension d, computed via the stability condition phi.

    If no phi is specified, it chooses a suitable small non-degenerate
    phi. For phiout=True, this phi is returned as part of the data.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import ModuliStableTropicalCurves, GraphStabilitySpace, logDR
        sage: V = GraphStabilitySpace(1,2)
        sage: phi = V(0,{(0,(1,2)):1/3, (1,):-1/10, (2,):1/10})
        sage: A = [2,-2]
        sage: tpart, pppart = logDR(1,A,2,phi)
        sage: M = ModuliStableTropicalCurves(1,2)
        sage: push = pppart.pushforward(M)
        sage: tpush = M.tautological_class(push)
        sage: t = tpart * tpush

    After pushforward to Mbar_1,2, this cycle should agree with the
    classical double ramification cycle::

        sage: from admcycles import DR_cycle
        sage: DR_cycle(1,A).vector()
        (0, 2, 2, 0, -1/24)
        sage: t.vector(1)
        (0, 2, 2, 0, -1/24)
        sage: DR_cycle(1,A,d=2).vector(2)
        (0, 0, 0, 0, 2, 4, 2, 0, 0, 0, 1/240, -1/12, -1/12, 0, -41/240)
        sage: t.vector(2)
        (0, 0, 0, 0, 2, 4, 2, 0, 0, 0, 1/240, -1/12, -1/12, 0, -41/240)

    We can also check a computation of a double-double ramification cycle::

        sage: B = [-6,6]
        sage: tpartB, pppartB = logDR(1,B,2,phi)
        sage: push = (pppart*pppartB).pushforward(M)
        sage: tpush = M.tautological_class(push)
        sage: t = tpart * tpartB * tpush
        sage: t.evaluate()
        -1/6
        sage: grcd = gcd(A[0],B[0])
        sage: (DR_cycle(1,[grcd,-grcd])*DR_cycle(1,[0,0])).evaluate()
        -1/6
    """
    n = len(Avector)
    M = ModuliStableTropicalCurves(g, n)
    if d is None:
        d = g
    if phi is None and sum(Avector) == 0:
        V = GraphStabilitySpace(g, n)
        LL = KassPagani_basis(g, n)
        # generates a small non-denenerate stability condition with probability 1
        phi = V(0, {LL[i]: choice([-1, 1]) * (2**(-g - n)) * random()
                for i in range(len(LL))})
        assert M.is_small(phi)
    k = ZZ(sum(Avector) / ZZ(2 * g - 2 + n))
    M = ModuliStableTropicalCurves(g, n)

    # the class eta
    R = TautologicalRing(g, n)
    eta = k**2 * R.kappa(1) - \
        sum(Avector[i]**2 * R.psi(i + 1) for i in range(n))
    # exp(-1/2 eta) computed up to codimension d
    expeta = (-ZZ.one() / 2 * eta).exp(d)

    # the class f_2
    f2 = M.second_PL_function(Avector, phi)
    expf2 = sum((ZZ.one() / 2 * f2)**j / factorial(j)
                for j in range(d + 1))

    # the class f_1
    f1 = M.first_PP_function(Avector, d, phi)

    # expeta=ZZ.one() #TODO: remove once products of taut. classes and PP functions work
    # TODO: for now return tautological and PP part separately
    if phiout:
        return (expeta, (expf2 * f1).truncate(d), phi)
    # TODO: extract codimension d part
    return (expeta, (expf2 * f1).truncate(d))


def logDR_subdivision(g, Avector, phi=None, phiout=False, complete=True, total_order=False):
    r"""
    Return the subdivision associated to the log double ramification cycle logDR_g(Avector)
    computed via the stability condition phi.

    If no phi is specified, it chooses (if necessary) a suitable small non-degenerate
    phi. For phiout=True, this phi is returned as part of the data.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import logDR_subdivision, GraphStabilitySpace
        sage: V = GraphStabilitySpace(1, 2)
        sage: phi = V(0,{(0,(1,2)):3.3, (1,):0.1, (2,):-0.1})
        sage: dic = logDR_subdivision(1, (3,-3), phi); dic
        {[0, 0] [[1, 2, 3], [4, 5, 6]] [(3, 4), (5, 6)]: Rational polyhedral fan in 2-d lattice N,
         [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)]: Rational polyhedral fan in 2-d lattice N}
        sage: for gamma, subdiv in dic.items():
        ....:     print(gamma)
        ....:     print(sorted(subdiv.rays()))
        ....:
        [0, 0] [[1, 2, 3], [4, 5, 6]] [(3, 4), (5, 6)]
        [N(0, 1), N(1, 0)]
        [0, 0] [[1, 3, 5], [2, 4, 6]] [(3, 4), (5, 6)]
        [N(0, 1), N(1, 0), N(1, 1), N(1, 2), N(1, 3), N(2, 1), N(3, 1)]
    """
    if complete and not total_order:
        tpart, pppart, phi = logDR(g, Avector, phi=phi, phiout=True)
        graphlist = pppart.get_fan().maxobjects()
        fanlist = pppart.get_fan().fans()
        if phiout:
            return {graphlist[i]: fanlist[i] for i in range(len(fanlist))}, phi
        else:
            return {graphlist[i]: fanlist[i] for i in range(len(fanlist))}
    if complete and total_order:
        return NotImplementedError
    if not complete and not total_order:
        n = len(Avector)
        M = ModuliStableTropicalCurves(g, n)
        graphs = M._maxgraphs
        mydict = {}
        for gamma in graphs:
            Pgamma = M.DRPicGraph(Avector, gamma)
            mydict[gamma] = Pgamma.incomplete_fan(full=True)
        return mydict
    if not complete and total_order:
        return NotImplementedError


def refines(self, other):
    r"""
    Returns whether the fan self refines other.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import refines
        sage: F1 = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
        sage: F2 = Fan([Cone([(1,0),(0,1)])])
        sage: refines(F1, F2)
        True
        sage: refines(F2, F1)
        False
    """
    return self.common_refinement(other) == self


sage.geometry.fan.RationalPolyhedralFan.refines = refines


def refinedicts(self, other):
    r"""
    For fan self refining the fan other, return dictionaries dic, dicinv, where
    dic sends maximal cones of self to the cones of other containing them, and dicinv
    sends maximal cones of other to the list of cones of self contained in them.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import refinedicts
        sage: F1 = Fan([Cone([(1,0),(1,1)]), Cone([(1,1),(0,1)])])
        sage: F2 = Fan([Cone([(1,0),(0,1)])])
        sage: dic, dicinv = refinedicts(F1, F2)
        sage: [(C.rays(), dic[C].rays()) for C in dic]
        [(N(1, 0),
          N(1, 1)
          in 2-d lattice N, N(1, 0),
          N(0, 1)
          in 2-d lattice N), (N(0, 1),
          N(1, 1)
          in 2-d lattice N, N(1, 0),
          N(0, 1)
          in 2-d lattice N)]
    """
    dic = {C: other.cone_containing(C) for C in self.cones(codim=0)}
    dicinv = defaultdict(list)
    for C in dic:
        dicinv[dic[C]].append(C)
    return dic, dict(dicinv)


def PL_from_ray_values(data):
    r"""
    Takes list data with entries of the form [vector, value] and computes the minimal
    fan and convex function on the span of all vectors that take the corresponding values
    at the vectors.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import PL_from_ray_values
        sage: PL_from_ray_values([[[1,0],0], [[0,1],0], [[1,1],-1]])
        Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
        (N(0, 1), N(1, 1)) : -x0
        (N(1, 0), N(1, 1)) : -x1
        sage: PL_from_ray_values([[[1,0,0],1], [[0,1,0],2], [[0,0,1],-3]])
        Piecewise polynomial on Rational polyhedral fan in 3-d lattice N
        (N(0, 0, 1), N(0, 1, 0), N(1, 0, 0)) : x0 + 2*x1 - 3*x2
        sage: PL_from_ray_values([[[1,0],1], [[0,1],1], [[0,-1],1], [[-1,0],1]])
        Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
        (N(-1, 0), N(0, 1)) : -x0 + x1
        (N(-1, 0), N(0, -1)) : -x0 - x1
        (N(0, -1), N(1, 0)) : x0 - x1
        (N(0, 1), N(1, 0)) : x0 + x1
    """
    dim = len(data[0][0])  # dimension of underlying fan
    zvect = vector(QQ, dim * [0] + [1])
    veclist = [vector(QQ, a + [b]) for a, b in data] + \
        [zvect]  # maybe [0,0,...,-1] here?
    C = Cone(veclist)
    lower_facet_data = [(c, n) for (c, n) in zip(
        C.facets(), C.facet_normals()) if zvect not in c]
    R = PolynomialRing(QQ, dim, 'x')
    x = R.gens()
    fan_data = {Cone([list(r)[:-1] for r in c.rays()]): (-1 / n[dim]) *
                sum(ai * xi for ai, xi in zip(n[:-1], x)) for c, n in lower_facet_data}
    F = Fan(fan_data.keys())
    S = PiecewisePolynomialRing(F)
    return S(fan_data)


def PL_from_fan_and_ray_values(F, data):
    r"""
    Takes a fan F and a list data with entries of the form [vector, value] and computes
    a piecewise linear function on the fan that take the corresponding values
    at the vectors. This assumes that each maximal cone of the fan contains sufficiently
    many of the vectors above to uniquely determine the result and that vectors are
    generators of the rays of the fan.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import PL_from_fan_and_ray_values
        sage: F = Fan([Cone([[1,0],[1,1]]), Cone([[1,1],[0,1]])])
        sage: PL_from_fan_and_ray_values(F, [[[1,0],0], [[0,1],0], [[1,1],-1]])
        Piecewise polynomial on Rational polyhedral fan in 2-d lattice N
        (N(1, 0), N(1, 1)) : -x1
        (N(0, 1), N(1, 1)) : -x0
        sage: V = [[0,0,1],[1,0,1],[0,1,1],[1,1,1],[2,0,1]]
        sage: F = Fan([Cone([V[0], V[1], V[2], V[3]]), Cone([V[1],V[3],V[4]])])
        sage: data = list(zip(V, [0,2,3,5,2]))
        sage: PL_from_fan_and_ray_values(F, data)
        Piecewise polynomial on Rational polyhedral fan in 3-d lattice N
        (N(0, 0, 1), N(0, 1, 1), N(1, 0, 1), N(1, 1, 1)) : 2*x0 + 3*x1
        (N(1, 0, 1), N(1, 1, 1), N(2, 0, 1)) : 3*x1 + 2*x2
        sage: data2 = list(zip(V, [0,2,3,6,2]))
        sage: PL_from_fan_and_ray_values(F, data2)
        Traceback (most recent call last):
        ...
        ValueError: Cone with the following rays contains contradictory data:
        [N(0, 0, 1), N(0, 1, 1), N(1, 0, 1), N(1, 1, 1)]
    """
    dim = F.dim()
    di = {}
    R = PolynomialRing(QQ, dim, 'x')
    x = R.gens()
    LL = F.lattice()
    for C in F.cones(codim=0):
        Cvecs = [[v, b] for v, b in data if LL(v) in C.rays()]
        A = matrix([v for v, _ in Cvecs])
        bv = vector([b for _, b in Cvecs])
        if A.rank() < dim:
            raise ValueError(
                'Cone with the following rays does not contain enough data:\n' + repr(sorted(C.rays())))
        try:
            sol = A.solve_right(bv)
        except ValueError:
            raise ValueError(
                'Cone with the following rays contains contradictory data:\n' + repr(sorted(C.rays())))
        di[C] = sum(ai * xi for ai, xi in zip(sol, x))
    S = PiecewisePolynomialRing(F)
    return S(di)


# from Aaron Pixton, modified by David Holmes
# it now allows flows with zeros in; no strict directed cycles. It *seems* to work. It produces a few duplicate flows, but never mind...
def weak_acyclic_flows(weights, legs, edges):
    r"""
    Calculates the 'weakly acyclic' flows on a graph, balancing out a given
    set of weights on the vertices (which must sum to zero).

    Here we mean
    that there does not exist a cycle in the graph on which all the flows
    point in the same direction with positive values. For example, on a
    'circle graph' with 2 edges, a flow which takes zero value on at least
    one of those edges is always weakly acyclic.

    INPUT:

    - weights: (list) list of integer weights on the vertices
    - legs: (list) list of legs of the vertices
    - edges: (list) list of pairs of legs connected by an edge

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import weak_acyclic_flows
        sage: weights = [5, -3, -2]
        sage: legs = [[1,3], [2,4,5], [6]]
        sage: edges = [(1,2), (3,4), (5,6)]
        sage: weak_acyclic_flows(weights, legs, edges)
        [{1: 5, 2: -5, 3: 0, 4: 0, 5: 2, 6: -2},
         {1: 4, 2: -4, 3: 1, 4: -1, 5: 2, 6: -2},
         {1: 3, 2: -3, 3: 2, 4: -2, 5: 2, 6: -2},
         {1: 2, 2: -2, 3: 3, 4: -3, 5: 2, 6: -2},
         {1: 1, 2: -1, 3: 4, 4: -4, 5: 2, 6: -2},
         {1: 0, 2: 0, 3: 5, 4: -5, 5: 2, 6: -2}]
    """
    def leg_in_edge(l):
        for e in edges:
            if l == e[0] or l == e[1]:
                return 1
        return 0
    # set of internal legs
    Linternal = [[l for l in L if leg_in_edge(l) == 1] for L in legs]
    Loutgoing = [[l for l in L if leg_in_edge(l) == 0] for L in legs]
    Loutgoing = [a for b in Loutgoing for a in b]
    Loutgoing = list(set(Loutgoing))  # set of markings
    # print("myinputs", (weights, [0 for i in range(len(weights))], Linternal, edges, {}))
    internal_dicts = weak_acyclic_recur(
        weights, [0 for i in range(len(weights))], Linternal, edges, {})
    # print("num", len(internal_dicts))
    nodup_internal_dicts = []
    for DD in internal_dicts:
        for L in Loutgoing:
            DD[L] = 0
        # print("111", DD)
        if DD not in nodup_internal_dicts:
            # print("222", DD)
            nodup_internal_dicts.append(DD)
    return nodup_internal_dicts

# notes on source_constraints logic:
# 1 if must be a source (connected by a 0 to a source)
# -2 if cannot be a source (skipped over when looking for earliest source)
# -1 if cannot be a source but will be eligible after 1's are processed
#    (connected by non-0 to a source)


def weak_acyclic_recur(weights, source_constraints, legs, edges, flow_dict):
    ans_list = []
    n = len(weights)  # number of vertices
    maxc = max(source_constraints)
    if maxc < 1:
        for i in range(n):
            if source_constraints[i] == -1:
                source_constraints[i] = 0
    done = True
    for i in range(n):
        if weights[i] < 0:
            continue
        if source_constraints[i] < 0:
            continue
        if maxc == 1 and source_constraints[i] < 1:
            continue
        Lincident = [x for x in legs[i] if x not in flow_dict]
        k = len(Lincident)
        if k == 0:
            continue
        done = False
        Lpair = []
        for x in Lincident:
            for e in edges:
                if e[0] == x:
                    Lpair.append(e[1])
                    break
                if e[1] == x:
                    Lpair.append(e[0])
                    break
        Lvert = []
        for l in Lpair:
            for j in range(n):
                if l in legs[j]:
                    Lvert.append(j)
                    break
        # non-negative integer vectors of length k summing to weights[i]
        for new_flow in IntegerVectors(weights[i], k):
            #   print(new_flow)
            violates_constraints = False
            for j in range(k):
                if new_flow[j] == 0:
                    if maxc < 1 and Lvert[j] < i or source_constraints[Lvert[j]] < 0:
                        # violates_constraints = True
                        break
                else:
                    if Lvert[j] == i or source_constraints[Lvert[j]] == 1:
                        # violates_constraints = True
                        break
        #   print("vc", violates_constraints)
            if violates_constraints:
                continue
        #   if not set([Lvert[j] for j in range(k) if new_flow[j] == 0]).isdisjoint(set([Lvert[j] for j in range(k) if new_flow[j] > 0])):
                # print("changed condition here", new_flow)
                # continue #removed this constraint.
            new_weights = copy(weights)
            new_constraints = copy(source_constraints)
            new_flow_dict = copy(flow_dict)
            if maxc < 1:
                for j in range(i):
                    new_constraints[j] = -2
            for j in range(k):
                x = new_flow[j]
                new_weights[Lvert[j]] += x
                new_flow_dict[Lincident[j]] = x
                new_flow_dict[Lpair[j]] = -x
                if x > 0:
                    new_constraints[Lvert[j]] = 1
                else:
                    new_constraints[Lvert[j]] = -1
            new_weights[i] = 0
            new_constraints[i] = 0
        #   print("inputs", (new_weights, new_constraints, legs, edges, new_flow_dict))
        #   print("ans_list", ans_list)
            ans_list += weak_acyclic_recur(new_weights,
                                           new_constraints, legs, edges, new_flow_dict)
        if maxc == 1:
            return ans_list
    if max(weights) == 0 and done:
        return [flow_dict]
    return ans_list


@cached_function
def cphiinv(C):
    r"""
    Return the rational function phi_C^(-1) associated to a cone C.

    EXAMPLES::

        sage: from admcycles.logtaut.picgraph import cphiinv
        sage: cphiinv(Cone([(1,0,0),(0,1,0),(1,0,1),(0,1,1)]))
        (-x0 - x1)/(-x0^2*x1*x2 - x0*x1^2*x2 + x0*x1*x2^2)
    """
    F = Fan([C])
    Fsimp = F.make_simplicial()
    R = PiecewisePolynomialRing(Fsimp)
    return sum(R.phi(D).polynomial(D)**(-1) for D in Fsimp.cones(codim=0))
