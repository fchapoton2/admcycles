.. linkall

.. _examples:

***************************
admcycles examples database
***************************

Below are list of example computations in ``admcycles`` with explanations. The associated Jupyter-notebooks can be downloaded from the 
`admcycles-gitlab repository <https://gitlab.com/modulispaces/admcycles/-/tree/master/docs/source/notebooks>`_.

Introductory presentations
==========================

.. toctree::
   :maxdepth: 1

   notebooks/Introduction to admcycles I (Les Diablerets 2022)
   notebooks/Introduction to admcycles II (Les Diablerets 2022)
   notebooks/admcycles - old and new (Les Diablerets 2023)

Calculations ordered by topics
==============================

Basic classes and intersection numbers
--------------------------------------

.. toctree::
   :maxdepth: 1

   notebooks/The string and dilaton equations
   notebooks/Hurwitz numbers and ELSV formulas

Tautological relations
----------------------

.. toctree::
   :maxdepth: 1

   notebooks/Getzler's relation (and symmetrizing tautological classes)

Logarithmic intersection theory and log-double ramification cycles
------------------------------------------------------------------

.. toctree::
   :maxdepth: 1

   notebooks/Logarithmic double ramification cycles


Papers with code
================

The following is a list of calculations which illustrate results from various papers.

.. toctree::
   :maxdepth: 1

   notebooks/Castorena-Gendron (2020) - On the locus of genus 3 curves that admit meromorphic differentials with a zero of order 6 and a pole of order 2
   notebooks/Faber (1995) - Intersection-theoretical computations on Mbar
   notebooks/Norbury (2017) - A new cohomology class on the moduli space of curves
   notebooks/Rossi-Buryak (2023) - Counting meromorphic differentials on CP1
   notebooks/Zograf (2020) - An explicit formula for Witten's 2-correlators
   notebooks/Lee-Tahar (2023) - One-dimensional strata of residueless meromorphic differentials

Contributors
============
The participants of the workshop `admcycles - coding intersection theory <https://sites.google.com/view/admcycles2022/home>`_ (Les Diablerets, 2022), in particular Danilo Lewański, Dimitrios Mitsios, Johannes Schmitt and Johannes Schwab.