.. _additivegeneratormodule:

Module ``diffstrata.additivegenerator``
========================================

.. automodule:: admcycles.diffstrata.additivegenerator
   :members:
   :undoc-members:
   :show-inheritance:
