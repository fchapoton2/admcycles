.. _auxiliarymodule:

Module ``diffstrata.auxiliary``
===============================

.. automodule:: admcycles.diffstrata.auxiliary
   :members:
   :undoc-members:
   :show-inheritance:
